extends Control

# un dico avec les valeurs des récompenses ?
const side_missions_text:Array = [
	"Récupérer l'objet aux voleurs pour le monsieur",
	"other"
]
const THEME = preload("res://Assets/new_theme.tres")

onready var labelContainer = $VBoxContainer

# Called when the node enters the scene tree for the first time.
func _ready():
	self.visible = false


func load_page():
	self.visible = true
	
	var label:Control
	for n in labelContainer.get_children(): # suppressions des anciens labels
		labelContainer.remove_child(n)
		n.queue_free()
	for i in range(0, PlayerInfo.side_missions.size()):
		if PlayerInfo.side_missions[i]==1:
			label = Label.new()
			label.text = self.side_missions_text[i]
			label.theme = THEME
			labelContainer.add_child(label)
	
	if labelContainer.get_child_count()==0:
		label = Label.new()
		label.text = "aucune missions secondaires"
		label.theme = THEME
		labelContainer.add_child(label)

#---------------------------------------------------------------------------------------------------
# button action
func _on_return_pressed():
	self.visible = false
