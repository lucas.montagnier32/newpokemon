extends Node

const researcher_array = [
	# nom, elemons_research_power, types_research_power, energieMax, buff, image
	["Researcher1",5,1,8,"pv","res://Assets/Pokemon/Bulbasaur.png"],
	["Researcher2",2,4,8,"pe","res://Assets/Pokemon/Bulbasaur.png"],
	["Researcher3",3,3,7,"att","res://Assets/Pokemon/Bulbasaur.png"],
	["Researcher4",0,6,8,"def","res://Assets/Pokemon/Bulbasaur.png"],
	["Researcher5",4,3,7,"spd","res://Assets/Pokemon/Bulbasaur.png"],
	["Researcher5",2,5,7,"crit","res://Assets/Pokemon/Bulbasaur.png"],
]


class Researcher_class:
	var id:int = 0
	var researcher_base_stats:Array
	# fin d'un combat: elemons_research_percent += elemons_research_power
	var elemons_research_percent:int = 0
	var types_research_percent:int = 0
	
	var current_energy:int
	
	func set_stats_from_id(passed_id:int) -> void:
		self.id = passed_id
		self.researcher_base_stats = researcher_array[id]
		self.current_energy = self.get_energyMax()
	
	func get_name() -> String:
		return self.researcher_base_stats[0]
	func get_elemons_research_power() -> int:
		return self.researcher_base_stats[1]
	func get_types_research_power() -> int:
		return self.researcher_base_stats[2]
	func get_energyMax() -> int:
		return self.researcher_base_stats[3]
	func get_buff() -> String:
		return self.researcher_base_stats[4]
	func get_image() -> String:
		return self.researcher_base_stats[5]
	
	
	func increase_research() -> void:
		if self.current_energy > 0:
			self.elemons_research_percent += self.get_elemons_research_power()
			self.types_research_percent += self.get_types_research_power()
			self.current_energy -= 1
			if self.elemons_research_percent>=100:
				PlayerInfo.elemons_research_points += 1
				self.elemons_research_percent = 0
			if self.types_research_percent>=100:
				PlayerInfo.types_research_points += 1
				self.types_research_percent = 0
	
	func heal_energy(e:int):
		self.current_energy += e
		if(self.current_energy > self.get_energyMax()):
			self.current_energy = self.get_energyMax()
	
	
	func save_Researcher(save_file:File):
		save_file.store_32(id)
		save_file.store_32(current_energy)
		save_file.store_32(elemons_research_percent)
		save_file.store_32(types_research_percent)
	
	func load_Researcher(save_file:File):
		self.id = save_file.get_32()
		self.current_energy = save_file.get_32()
		self.elemons_research_percent = save_file.get_32()
		self.types_research_percent = save_file.get_32()
		# init des stats de base
		self.researcher_base_stats = researcher_array[id]
