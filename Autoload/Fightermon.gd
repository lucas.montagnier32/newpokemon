extends Node

const NB_CAPA_MAX = 4

const efficiencies = [
	["mét", "air", "bes", "eau", "ele", "feu", "gla", "mag", "obs", "vég", "sab", "roc"],
	["métal   ", 1 , 1 , 1 ,0.5, 1 , 1 ,1.5, 1 , 1 ,  1,0.5,1.5],
	["air     ", 1 , 1 , 1 , 1 , 1 ,0.5, 1 , 1 , 1 ,1.5, 1 , 1 ],
	["bestial ", 1 , 1 , 1 , 1 , 1 , 1 , 1 , 1 , 1 ,1.5, 1 ,0.5],
	["eau     ",1.5, 1 , 1 , 1 ,0.5,1.5,0.5, 1 , 1 ,0.5,1.5, 1 ],
	["electrik", 1 , 1 , 1 ,1.5, 1 , 1 , 1 , 1 ,0.5, 1 , 1 , 1 ],
	["feu     ", 1 ,1.5, 1 ,0.5, 1 , 1 ,1.5, 1 , 1 ,1.5,0.5, 1 ],
	["glace   ",0.5, 1 , 1 ,1.5, 1 ,0.5, 1 , 1 , 1 , 1 , 1 , 1 ],
	["magie   ", 1 , 1 , 1 , 1 , 1 , 1 , 1 , 1 , 1 , 1 , 1 , 1 ],
	["obscur  ", 1 , 1 , 1 , 1 ,1.5, 1 , 1 ,0.5, 1 , 1 , 1 , 1 ],
	["végétal ", 1 ,0.5,0.5,1.5, 1 ,0.5, 1 , 1 , 1 , 1 , 1 ,1.5],
	["sable   ",1.5, 1 , 1 ,0.5, 1 ,1.5, 1 , 1 , 1 , 1 , 1 , 1 ],
	["roche   ",0.5, 1 ,1.5, 1 , 1 , 1 , 1 , 1 , 1 ,0.5, 1 , 1 ],
]

# tableau de tout les Elemons et leur stats de base constant
const figthermons_array = [
	# nom, type1, type2, pvmax, energiemax, att, def, attspe, defspe, vit, type d'evolution
	["Creusol","roche","",           10,20,16,12,13,11,7,"E","res://Assets/Pokemon/Pokemon10.png"],
	["Creusoche","roche","",         20,30,24,17,19,18,10,"E","res://Assets/Pokemon/Pokemon11.png"],
	["Creusarche","roche","magie",   30,40,28,22,23,21,15,"E","res://Assets/Pokemon/Pokemon12.png"],
	["Tigro","bestial","",           10,20,14,14,14,14,6,"F","res://Assets/Pokemon/Pokemon13.png"],
	["Rochat","roche","",            20,30,20,20,12,13,9,"F","res://Assets/Pokemon/Pokemon14.png"],
	["Tigrochat","bestial","roche",  30,40,25,25,23,21,16,"F","res://Assets/Pokemon/Pokemon15.png"],
	["Minerir","roche","",           12,18,12,16,11,14,5,"E","res://Assets/Pokemon/Pokemon17.png"],
	["Minerare","roche","",          24,26,17,24,15,20,8,"E","res://Assets/Pokemon/Pokemon18.png"],
	["Miracieux","roche","",         36,34,22,28,20,25,15,"E","res://Assets/Pokemon/Pokemon19.png"],
	["Brinderbe","vegétal","",       10,20,12,16,11,14,6,"E","res://Assets/Pokemon/Pokemon17.png"],
	["Brancherbe","vegétal","",      20,30,17,24,15,20,9,"E","res://Assets/Pokemon/Pokemon18.png"],
	["Brancharbre","vegétal","magie",30,40,22,28,20,25,16,"E","res://Assets/Pokemon/Pokemon19.png"]
]

# pour elemon d'indice i, il a les capcités suivantes
const capa_de_base_ennemi_array = [
	[0,1],
	[0,2],
	[0,2,3,4],
	[0,1],
	[0,4],
	[0,2,3,5],
	[2,3],
	[2,3,5],
	[1,2,3,4],
	[0,1],
	[0,2],
	[0,2,3,4],
]

# array des capacite
# 1=mét, 2=air, 3=bes, 4=eau, 5=ele, 6=feu, 7=gla, 8=mag, 9=obs, 10=vég, 11=sab, 12=roc
const capacites_fight = [
	# nom, type, is_physique, percent_degat, pe utilisé
	["Charge","bestial", true, 50, 2],
	["Herbe volante","vegétal", true, 40, 2],
	["Illusion","magie", true, 60, 3],
	["Bulle d'eau","eau", false, 65, 3],
	["Éclair","electrik", true, 70, 3],
	["Souffle de feu","feu", false, 60, 2],
	["Inutile","bestial", false, 0, 2],
	["Illusion destructrice","magie", false, 150, 5]
]

enum rarity_enum {WEAK, NORMAL, STRONG, POWERFUL, LEGENDARY}

# retourne un entier correspondant au type passé en paramètre
func stringCapa_to_int(stringCapa:String):
	var replacedStringCapa:String = stringCapa.replace("é","e")
	var rep:int = 100
	match replacedStringCapa:
		"met","metal": rep=1
		"air": rep=2
		"ani","animal","bes","bestial": rep=3
		"eau": rep=4
		"ele","elec","electrik","electrique": rep=5
		"feu": rep=6
		"gla","glace": rep=7
		"mag","magie": rep=8
		"obs","obscur": rep=9
		"veg","vegetal": rep=10
		"sab","sable": rep=11
		"roc","roche": rep=12
		_: print("error in stringCapa_to_int: stringCapa="+stringCapa)
	return rep

class Fightermon_class:
	
	var id:int
	var rarity:int
	
	var pvmax:int
	var pemax:int
	var att:int
	var def:int
	var attspe:int
	var defspe:int
	var speed:int
	var probaCrit:int # entre 0 et 100
	var degatsCrit:float # mutiplicateur de degats
	
	# stats augmentées choisi par le joueur
	# sert juste pour l'affichage dans les stats des elemons
	# ne pas additionner avec les stats de stats qui ont déja été augmenter
	var pvmaxBonus:int
	var pemaxBonus:int
	var attBonus:int
	var defBonus:int
	var attspeBonus:int
	var defspeBonus:int
	var speedBonus:int
	
	var current_pv:int
	var current_pe:int
	
	# on dit que le niveau
	var level:int
	var xp:int
	# a chaque monté de niveau de xp_spe le joueur peut choisir une stat a améliorer
	var level_spe:int
	var xp_spe:int
	
	var idcapas:Array
	
	# meme forme que le tableau ci-dessus figthermons_array
	var all_stats:Array
	# tableau de 4 tableaux corresondant aux capacites_fight ci_dessus
	var capacites:Array
	
	
	func get_name(): return all_stats[0]
	func get_type1(): return all_stats[1]
	func get_type2(): return all_stats[2]
	func get_pvmax(): return self.pvmax
	func get_pemax(): return self.pemax
	func get_att(): return self.att
	func get_def(): return self.def
	func get_attspe(): return self.attspe
	func get_defspe(): return self.defspe
	func get_speed(): return self.speed
	func get_evolveType(): 
		match all_stats[10]:
			"E","e": return "Évolution"
			"F","f": return "Fusion"
	func get_image(): return all_stats[11]
	
	func get_string_rarity() -> String:
		match self.rarity:
			0: return "★"
			1: return "★★"
			2: return "★★★"
			3: return "★★★★"
			4: return "★★★★★"
			_: return "error on get_string_rarity"
	
	func get_string_eval() -> String:
		return "Puissance: "+str(pvmax+pemax+att+def+attspe+defspe+speed)
	
	# charge les stats du fightermon en fonction de son id
	func set_stats_from_id(id_f:int) -> void:
		# (true) a deep copy is performed: all nested arrays and dictionaries are duplicated and will not be shared with the original array
		self.all_stats = figthermons_array[id_f]
		self.id = id_f
		self.level = 1
		self.xp = 0
		self.level_spe = 0
		self.xp_spe = 0
		self.pvmax = all_stats[3]*(0.9+rarity*0.1)
		self.pemax = all_stats[4]*(0.9+rarity*0.1)
		self.att = all_stats[5]*(0.9+rarity*0.1)
		self.def = all_stats[6]*(0.9+rarity*0.1)
		self.attspe = all_stats[7]*(0.9+rarity*0.1)
		self.defspe = all_stats[8]*(0.9+rarity*0.1)
		self.speed = all_stats[9]*(0.9+rarity*0.1)
		self.probaCrit = 5+rarity
		self.degatsCrit = 2
	
	
	# charge la capa d'id id_capa capacite[i]
	func set_capa_from_id(id_capa:int, i:int):
		assert(i>=0 && i<=3)
		if(self.capacites.size()>=4):# si toutes ces capacités sont prise on la remplace
			self.capacites[i] = capacites_fight[id_capa]
			self.idcapas[i] = id_capa
		else:# sinon on la met en dernière position
			self.capacites.push_back(capacites_fight[id_capa])
			self.idcapas.push_back(id_capa)
	
	# charge la capa d'id id_capa capacite[i]
	func set_capa_from_id_without_set_idpacas(id_capa:int, i:int):
		assert(i>=0 && i<=3)
		if(self.capacites.size()>=4):# si toutes ces capacités sont prise on la remplace
			self.capacites.insert(i, capacites_fight[id_capa])
		else:# sinon on la met en dernière position
			self.capacites.insert(capacites.size(), capacites_fight[id_capa])
	
	# met le level a lvl et change les stats en consequences
	func set_level_and_change_stats(lvl:int):
		self.level = lvl
		self.pvmax += 2*lvl
		self.pemax += lvl
		self.att += lvl
		self.def += lvl
		self.attspe += lvl
		self.defspe += lvl
		self.speed += lvl
		self.current_pv += 2*lvl
		self.current_pe += lvl
	
	
	func set_stats_from_id_and_level(id_f:int, lvl:int, rari:int):
		self.rarity = rari
		set_stats_from_id(id_f)
		set_level_and_change_stats(lvl)
		self.current_pv = self.pvmax
		self.current_pe = self.pemax
	
	func add_one_level():
		self.level += 1
		self.pvmax += 2
		self.pemax += 1
		self.att += 1
		self.def += 1
		self.attspe += 1
		self.defspe += 1
		self.speed += 1
		self.current_pv += 2
		self.current_pe += 1
	
	# ajoute l'xp a l'xp de ce pokemon renvoie vrai si monte de niveau
	func add_xp(val:int):# DEAD CODE ?
		assert(val>=0)
		self.xp += val
		if(self.xp>=calculate_next_level_xp()):# level++
			self.xp = self.xp-calculate_next_level_xp()# revient a 0 en gros
			self.level += 1
			for i in range(3,10):
				self.all_stats[i] += 1
			return true
		return false
	
	# ajoute l'xp a l'xp de ce pokemon renvoie vrai si monte de niveau
	func set_xp(val:int) -> bool:
		assert(val>=0)
		if(self.level<PlayerInfo.get_elemonLevelLimit()):
			self.xp = val
			if(self.xp>=calculate_next_level_xp()):# level++
				self.xp = self.xp-calculate_next_level_xp()# revient a 0 en gros
				self.add_one_level()
				return true
		return false
	
		# calcul le nombre d'xp pour le prochain niveau
	func calculate_next_level_xp() -> int:
		return self.level*40
	
	# ajoute l'xp a l'xp de ce pokemon renvoie vrai si monte de niveau
	func add_xp_spe(val:int):# DEAD CODE ?
		assert(val>=0)
		self.xp_spe += val
		if(self.xp_spe>=calculate_next_level_xp_spe()):# level++
			self.xp_spe = self.xp_spe-calculate_next_level_xp_spe()# revient a 0 en gros
			self.level_spe += 1
			return true
		return false
	
	# ajoute l'xp a l'xp de ce pokemon renvoie vrai si monte de niveau
	func set_xp_spe(val:int):
		assert(val>=0)
		self.xp_spe = val
		if(self.xp_spe>=calculate_next_level_xp_spe()):# level++
			self.xp_spe = self.xp_spe-calculate_next_level_xp_spe()# revient a 0 en gros
			self.level_spe += 1
			return true
		return false
	
	# calcul le nombre d'xp pour le prochain niveau
	func calculate_next_level_xp_spe():
		return self.level_spe*25+25
	
	# calcul le nombre d'xp gagné quand self est mis K.O
	func calculate_xp_when_ko():
		var res_xp = 0
		for i in range (3,10):
			res_xp += self.all_stats[i]
		return (res_xp/2)
	
	func heal_max():
		self.current_pv = self.pvmax
		self.current_pe = self.pemax
	
	func heal_pv(pv:int):
		self.current_pv += pv
		if(self.current_pv > self.pvmax):
			self.current_pv = self.pvmax
	
	func heal_pe(pe:int):
		self.current_pe += pe
		if(self.current_pe > self.pemax):
			self.current_pe = self.pemax
	
	# sauvegarde d'un pokemon
	func save_mon(save_file:File):
		save_file.store_32(id)
		save_file.store_32(rarity)
		save_file.store_32(pvmax)
		save_file.store_32(pemax)
		save_file.store_32(att)
		save_file.store_32(def)
		save_file.store_32(attspe)
		save_file.store_32(defspe)
		save_file.store_32(speed)
		save_file.store_32(probaCrit)
		save_file.store_float(degatsCrit)
		save_file.store_32(pvmaxBonus)
		save_file.store_32(pemaxBonus)
		save_file.store_32(attBonus)
		save_file.store_32(defBonus)
		save_file.store_32(attspeBonus)
		save_file.store_32(defspeBonus)
		save_file.store_32(speedBonus)
		save_file.store_32(current_pv)
		save_file.store_32(current_pe)
		save_file.store_32(level)
		save_file.store_32(xp)
		save_file.store_32(level_spe)
		save_file.store_32(xp_spe)
		
		save_file.store_32(self.capacites.size())
		for i in range (0,self.capacites.size()):
			save_file.store_32(idcapas[i])
	
	func load_mon(save_file:File):
		id = save_file.get_32()
		rarity = save_file.get_32()
		pvmax = save_file.get_32()
		pemax = save_file.get_32()
		att =save_file.get_32()
		def = save_file.get_32()
		attspe = save_file.get_32()
		defspe = save_file.get_32()
		speed = save_file.get_32()
		probaCrit = save_file.get_32()
		degatsCrit = save_file.get_float()
		pvmaxBonus = save_file.get_32()
		pemaxBonus = save_file.get_32()
		attBonus =save_file.get_32()
		defBonus = save_file.get_32()
		attspeBonus = save_file.get_32()
		defspeBonus = save_file.get_32()
		speedBonus = save_file.get_32()
		current_pv = save_file.get_32()
		current_pe = save_file.get_32()
		level = save_file.get_32()
		xp = save_file.get_32()
		level_spe = save_file.get_32()
		xp_spe = save_file.get_32()
		
		var nb_capas = save_file.get_32()
		idcapas.clear()
		for i in range (0,nb_capas):
			idcapas.insert(i,save_file.get_32())
		
		# remise aux stats de bases
		capacites.clear()
		for i in range(0,nb_capas):
			self.set_capa_from_id_without_set_idpacas(idcapas[i],1)
		self.all_stats = figthermons_array[id]
