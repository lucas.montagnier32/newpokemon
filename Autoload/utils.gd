extends Node
# script qu'on peut utiliser partout

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func get_player():
	return get_node("/root/SceneManager/CurrentScene").get_children().back().find_node("Player")

func get_scene_manager():
	return get_node("/root/SceneManager")

func get_GUI():
	return get_node("/root/SceneManager/GUI")

func player_won_objects(objects:String):
	return get_node("/root/SceneManager/WinObjects").player_won_objects(objects)

func get_trainer_from_id(id:int):
	var trainer = "Trainer"+str(id)
	return get_node("/root/SceneManager/CurrentScene").get_children().back().find_node(trainer)

func get_pnj_from_id(id:int):
	var pnj = "PNJ"+str(id)
	return get_node("/root/SceneManager/CurrentScene").get_children().back().find_node(pnj)

func initiate_dialog(dialogs:Array):
	get_node("/root/SceneManager/DialogBox").load_dialog(dialogs)
