extends Node

const missions:Array = [
	"Tes parents doivent te parler, iels t'attendent en bas.",
	"",
	"Tu peux aller visiter le labo de tes parents au nord-ouest pour trouver tes premiers elemons.",
	"Tu peux maintenant explorer le monde",
	"Battre le gardien de la porte du lac"
]
const NUMBER_OF_RESEARCH = 10
const NUMBER_OF_SIDE_MISSIONS = 1

var altitude:int
var claimed_map_bags:Array # les id des sacs récupéré sur le terrain
var current_mission:int
var current_scene:String
var money:int
var nb_elemons_f:int # nombre d'elemons
var nb_researcher:int
var player_name:String
var player_rank:int
var player_rank_xp:int
var position:Vector2
var side_missions:Array # missions secondaires: 0 si pas encore vu, 1 si en cours, 2 si fini

# 4 Elemon de l'équipe du joueur
var elemon11:Fightermon.Fightermon_class
var elemon12:Fightermon.Fightermon_class
var elemon13:Fightermon.Fightermon_class
var elemon14:Fightermon.Fightermon_class
var elemon15:Fightermon.Fightermon_class

# 3 Elemon spé de l'équipe du joueur
var researcher1:Researcher.Researcher_class
var researcher2:Researcher.Researcher_class
var researcher3:Researcher.Researcher_class

# sac du joueur ("keyName":number)
var bag_treatment:Dictionary = {
	"pv20":5, "pv40":20, "pv80":0, "pv160":0, "pvmax":0, 
	"pe10":4, "pe20":1, "pe40":1, "pe80":0, "pemax":0,
}
# ("keyName":power)
var bag_treatment_power:Dictionary = {
	"pv20":20, "pv40":40, "pv80":80, "pv160":160, "pvmax":9999,
	"pe10":10, "pe20":20, "pe40":40, "pe80":80, "pemax":9999,
}
var bag_gifts:Dictionary = {
	"petit":5, "moyen":2, "bon":1, "grand":1, "génial":1, "superbe":1,
}
var bag_gifts_power:Dictionary = {
	"petit":30, "moyen":40, "bon":50, "grand":60, "génial":70, "superbe":80,
}
var bag_capacites:Dictionary = { # "tech":id_capas
	"tech1":0,
	"tech2":1,
	"tech4":3,
}
var bag_objects:Dictionary = {
	"key1":1,
	"waste":18,
	"collier":0,
}
var bag_utility:Dictionary = {
	"efficacités":1,
}

# research graph
var player_research_points:int
var elemons_research_points:int
var types_research_points:int
var player_bool_research_array:Array = []
var elemons_bool_research_array:Array = []
var types_bool_research_array:Array = []
# player research var
var charisma_gain:int
var levelLimit_gain:int
var player_xp_gain:int
var healing_speed_gain:int
# elemons research var
var xp_gain:float
var xp_spe_gain:float
var pv_gain:int
var pe_gain:int
# types research var
var damage_capa_elemon_same_types:float
var damage_super_efficience:float
var ennemi_damage_super_efficience:float
var ennemi_damage_capa_elemon_same_types:float

# le dresseur est battu si son id apparait dans ce tableau
var defeatedTrainerId:Array = []

# Called when the node enters the scene tree for the first time.
func _ready():
	# quand probleme avec le fichier de sauvegarde faire un (true || ...)
	if(not PlayerInfo.exist_load()):
		#old_ready()
		real_ready()
	else:
		self.load_data()
	self.update_player_research_bonus()
	self.update_elemons_research_bonus()
	self.update_types_research_bonus()


func old_ready():
	elemon11 = Fightermon.Fightermon_class.new()
	elemon11.set_stats_from_id_and_level(1, 6, Fightermon.rarity_enum.STRONG)
	elemon11.set_capa_from_id(3,0)
	elemon11.set_capa_from_id(4,0)
	elemon11.set_capa_from_id(6,0)
	elemon11.set_capa_from_id(7,0)
	elemon12 = Fightermon.Fightermon_class.new()
	elemon12.set_stats_from_id_and_level(5, 7, Fightermon.rarity_enum.STRONG)
	elemon12.set_capa_from_id(3,0)
	elemon12.set_capa_from_id(6,0)
	elemon12.set_capa_from_id(7,0)
	nb_elemons_f = 2
	player_rank = 1
	current_mission = 1

func real_ready():
	nb_elemons_f = 0
	nb_researcher = 0
	player_rank = 1
	current_mission = 1
	player_research_points = 11 # a mettre a 0
	elemons_research_points = 11 # a mettre a 0
	types_research_points = 11 # a mettre a 0
	for _i in range(0, NUMBER_OF_SIDE_MISSIONS):
		side_missions.push_back(0)
	for _i in range(0, NUMBER_OF_RESEARCH):
		player_bool_research_array.push_back(false)
		elemons_bool_research_array.push_back(false)
		types_bool_research_array.push_back(false)
	# warning-ignore:return_value_discarded
	add_new_researcher_in_the_player_team(0) # a supprimer

# update des bonus de recherche pour chaques branches
func update_player_research_bonus() -> void:
	self.charisma_gain = 1 if player_bool_research_array[0] else 0
	self.levelLimit_gain = 2 if player_bool_research_array[1] else 0
	self.player_xp_gain = 1 if player_bool_research_array[2] else 0
	self.healing_speed_gain = 1 if player_bool_research_array[3] else 0

func update_elemons_research_bonus() -> void:
	self.xp_gain = 1.05 if elemons_bool_research_array[0] else 1.0
	self.xp_spe_gain = 1.05 if elemons_bool_research_array[1] else 1.0
	self.pv_gain = 3 if elemons_bool_research_array[2] else 0
	self.pe_gain = 2 if elemons_bool_research_array[3] else 0

func update_types_research_bonus() -> void:
	self.damage_capa_elemon_same_types = 0.05 if types_bool_research_array[0] else 0.0
	self.damage_super_efficience = 0.05 if types_bool_research_array[1] else 0.0
	self.ennemi_damage_super_efficience = 0.05 if types_bool_research_array[2] else 0.0
	self.ennemi_damage_capa_elemon_same_types = 0.05 if types_bool_research_array[3] else 0.0


func heal_all_max():
	for e in get_all_elemons():
		if e!=null:
			e.heal_max()
	for r in get_all_researcher():
		if r!=null:
			r.current_energy = r.get_energyMax()

func heal_all_by(pv:int, pe:int, e:int):
	for e in self.get_all_elemons():
		if e!=null:
			e.heal_pv(pv)
			e.heal_pe(pe)
	for r in self.get_all_researcher():
		if r!=null:
			r.heal_energy(e)


func get_convinceAbility() -> int:
	return self.player_rank*4+self.charisma_gain

func get_elemonLevelLimit() -> int:
	return self.player_rank*8+self.levelLimit_gain

func get_elemon(elemon_number):
	match elemon_number:
		1: return elemon11
		2: return elemon12
		3: return elemon13
		4: return elemon14
		5: return elemon15
		_: print("error in get_elemon("+str(elemon_number)+")")

func get_all_elemons() -> Array:
	return [elemon11, elemon12, elemon13, elemon14, elemon15]

func get_all_researcher() -> Array:
	return [researcher1, researcher2, researcher3]

# buff de 15% pour chaque chercheur ayant le buff
func get_researcher_buff(buff:String) -> float:
	var mult:float = 1.0
	for r in get_all_researcher():
		if r != null && r.get_buff() == buff:
			mult += 0.15
	return mult

func researcher_buff_to_string() -> String:
	var buffString = "Buff en combat: "
	for r in get_all_researcher():
		if r != null:
			buffString += " "+r.get_buff()+" +"+str(self.get_researcher_buff(r.get_buff())*100-100)+"% "
	return buffString


func calculate_next_level_rank() -> int:
	return self.player_rank*15

# ajoute l'xp a l'xp du joueur renvoie vrai si monte de niveau
func add_xp_rank(val:int) -> void:
	assert(val>=0)
	self.player_rank_xp += val+self.player_xp_gain
	if(self.player_rank_xp>=calculate_next_level_rank()):# rank++
		self.player_rank_xp = self.player_rank_xp-calculate_next_level_rank()# revient a 0 en gros
		self.player_rank += 1
		self.player_research_points += 2


# renvoie vrai si une place a été trouvé dans l'équipe, faux sinon
func add_new_elemon_in_the_player_team(id:int, lvl:int, rarity:int, idcapas:Array) -> bool:
	var new_elemon:String = first_null_elemon()
	if new_elemon!="":
		nb_elemons_f += 1
		set(new_elemon, Fightermon.Fightermon_class.new())
		get(new_elemon).set_stats_from_id_and_level(id, lvl, rarity)
		# research bonus
		get(new_elemon).pvmax += self.pv_gain
		get(new_elemon).current_pv += self.pv_gain
		get(new_elemon).pemax += self.pe_gain
		get(new_elemon).current_pe += self.pe_gain
		for i in idcapas:
			get(new_elemon).set_capa_from_id(i,0)
		return true
	else: 
		print("pas de places dans l'équipe")
		return false

func first_null_elemon() -> String:
	if elemon11==null: return "elemon11"
	elif elemon12==null: return "elemon12"
	elif elemon13==null: return "elemon13"
	elif elemon14==null: return "elemon14"
	elif elemon15==null: return "elemon15"
	else: return ""


# researcher
func add_new_researcher_in_the_player_team(id:int) -> bool:
	var new_researcher:String = first_null_researcher()
	if new_researcher!="":
		nb_researcher += 1
		# get et set permetent de retrouver une variable à partir d'une String
		set(new_researcher, Researcher.Researcher_class.new())
		get(new_researcher).set_stats_from_id(id)
		return true
	else:
		print("pas de places dans l'équipe de recherche")
		return false

func first_null_researcher() -> String:
	if researcher1==null: return "researcher1"
	elif researcher2==null: return "researcher2"
	elif researcher3==null: return "researcher3"
	else: return ""

func researcher_increase_percent():
	for r in self.get_all_researcher():
		if r!=null:
			r.increase_research()


func save_data():
	var save_file = File.new()
	save_file.open("user://savefile.save", File.WRITE)
	
	save_file.store_var(current_scene)
	save_file.store_32(current_mission)
	save_file.store_64(money)
	self.position = Utils.get_player().position
	save_file.store_var(position)
	save_file.store_var(player_name)
	save_file.store_64(player_rank)
	save_file.store_64(player_rank_xp)
	save_file.store_var(side_missions, true)
	save_file.store_var(claimed_map_bags, true)
	
	# save research
	save_file.store_64(player_research_points)
	save_file.store_64(elemons_research_points)
	save_file.store_64(types_research_points)
	save_file.store_var(player_bool_research_array, true)
	save_file.store_var(elemons_bool_research_array, true)
	save_file.store_var(types_bool_research_array, true)
	
	# save bag
	for value in self.bag_treatment.values():
		save_file.store_32(value)
	for value in self.bag_gifts.values():
		save_file.store_32(value)
	for value in self.bag_objects.values():
		save_file.store_32(value)
	
	# save mon
	save_file.store_32(nb_elemons_f)
	if elemon11 != null:
		elemon11.save_mon(save_file)
	if elemon12 != null:
		elemon12.save_mon(save_file)
	if elemon13 != null:
		elemon13.save_mon(save_file)
	if elemon14 != null:
		elemon14.save_mon(save_file)
	if elemon15 != null:
		elemon15.save_mon(save_file)
	
	# save researcher
	save_file.store_32(nb_researcher)
	if researcher1 != null:
		researcher1.save_Researcher(save_file)
	if researcher2 != null:
		researcher2.save_Researcher(save_file)
	if researcher3 != null:
		researcher3.save_Researcher(save_file)
	
	# save trainer defated
	save_file.store_64(defeatedTrainerId.size())
	for i in defeatedTrainerId:
		save_file.store_64(i)
	
	save_file.close()
	print("claimed_map_bags:")
	print(claimed_map_bags)
	print("defeatedTrainerId:")
	print(defeatedTrainerId)
	print("--- Saved ---")


func load_data():
	var save_file = File.new()
	if not save_file.file_exists("user://savefile.save"):
		return
	save_file.open("user://savefile.save", File.READ)
	
	current_scene = save_file.get_var()
	current_mission = save_file.get_32()
	money = save_file.get_64()
	position = save_file.get_var()
	player_name = save_file.get_var()
	player_rank = save_file.get_64()
	player_rank_xp = save_file.get_64()
	side_missions = save_file.get_var(true)
	claimed_map_bags = save_file.get_var(true)
	
	# load research
	player_research_points = save_file.get_64()
	elemons_research_points = save_file.get_64()
	types_research_points = save_file.get_64()
	player_bool_research_array = save_file.get_var(true)
	elemons_bool_research_array = save_file.get_var(true)
	types_bool_research_array = save_file.get_var(true)
	
	# load bag
	for key in bag_treatment.keys():
		self.bag_treatment[key] = save_file.get_32()
	for key in bag_gifts.keys():
		self.bag_gifts[key] = save_file.get_32()
	for key in bag_objects.keys():
		self.bag_objects[key] = save_file.get_32()
	
	# load mon
	nb_elemons_f = save_file.get_32()
	if (nb_elemons_f>0):
		elemon11 = Fightermon.Fightermon_class.new()
		elemon11.load_mon(save_file)
	if (nb_elemons_f>1):
		elemon12 = Fightermon.Fightermon_class.new()
		elemon12.load_mon(save_file)
	if (nb_elemons_f>2):
		elemon13 = Fightermon.Fightermon_class.new()
		elemon13.load_mon(save_file)
	if (nb_elemons_f>3):
		elemon14 = Fightermon.Fightermon_class.new()
		elemon14.load_mon(save_file)
	if (nb_elemons_f>4):
		elemon15 = Fightermon.Fightermon_class.new()
		elemon15.load_mon(save_file)
	
	# save researcher
	nb_researcher = save_file.get_32()
	if nb_researcher>0:
		researcher1 = Researcher.Researcher_class.new()
		researcher1.load_Researcher(save_file)
	if nb_researcher>1:
		researcher2 = Researcher.Researcher_class.new()
		researcher2.save_Researcher(save_file)
	if nb_researcher>2:
		researcher3 = Researcher.Researcher_class.new()
		researcher3.save_Researcher(save_file)
	
	#load trainer defeated
	var taille = save_file.get_64()
	for _i in range(0,taille):
		defeatedTrainerId.push_back(save_file.get_64())
	
	save_file.close()
	print("claimed_map_bags:")
	print(claimed_map_bags)
	print("defeatedTrainerId:")
	print(defeatedTrainerId)
	print("--- Loaded ---")


# vrai si il existe un fichier de sauvegarde
func exist_load():
	var save_file = File.new()
	return save_file.file_exists("user://savefile.save")

func remove_save_file():
	var dir = Directory.new()
	dir.remove("user://savefile.save")
	print("--- Deleted ---")
