extends Control

var chosen_id:int

# Called when the node enters the scene tree for the first time.
func _ready():
	self.visible = false
	$down/text/Name1.text = Fightermon.figthermons_array[0][0]+"\nBon attaquant"
	$down/text/Name2.text = Fightermon.figthermons_array[3][0]+"\nÉquilibré"
	$down/text/Name3.text = Fightermon.figthermons_array[6][0]+"\nBon défenseur"


# fonction appelé depuis GUI.gd
func load_page():
	self.visible = true


# le joueur a choisi un starter
func _on_Button_pressed(id_ele:int):
	chosen_id = id_ele
	$Confirm.dialog_text = "Demander à "+Fightermon.figthermons_array[chosen_id][0]+" de te rejoindre ?"
	$Confirm.popup_centered()

# le joueur a choisi son starter
func _on_Confirm_confirmed():
	# warning-ignore:return_value_discarded
	PlayerInfo.add_new_elemon_in_the_player_team(chosen_id, 5, Fightermon.rarity_enum.STRONG, [0])
	self.visible = false
	Utils.get_scene_manager().transition_to_trainer_battle(4, false, [Vector2((chosen_id+3)%6, 3)], -1, Fightermon.rarity_enum.WEAK)
