extends CanvasLayer

# get access to menu
onready var menu = $Menu/Control
# access to pause button
onready var pause_button = $Pause/PauseButton

onready var elemonLabel1 = $Menu/Control/ControlMenu/menu/VBoxContainerLabel/elemenu/elemons/elemonLabel1
onready var elemonLabel2 = $Menu/Control/ControlMenu/menu/VBoxContainerLabel/elemenu/elemons/elemon23/elemonLabel2
onready var elemonLabel3 = $Menu/Control/ControlMenu/menu/VBoxContainerLabel/elemenu/elemons/elemon23/elemonLabel3
onready var elemonLabel4 = $Menu/Control/ControlMenu/menu/VBoxContainerLabel/elemenu/elemons/elemon45/elemonLabel4
onready var elemonLabel5 = $Menu/Control/ControlMenu/menu/VBoxContainerLabel/elemenu/elemons/elemon45/elemonLabel5
onready var researcherLabels = $Menu/Control/ControlMenu/menu/VBoxContainerLabel/cherchomon.get_children()

# trois états dans lesquelles les input vont etre traiter différamment
enum ScreenLoaded { NOTHING, JUST_MENU }
var screen_loaded = ScreenLoaded.NOTHING # rien d'afficher au début

#---------------------------------------------------------------------------------------------------
# Called when the node enters the scene tree for the first time.
func _ready():
	$Pause.visible = true
	menu.visible = false
	update_GUI()


#---------------------------------------------------------------------------------------------------
# quand le bouton pause est appuiyé
func _on_TextureButton_pressed():
	match screen_loaded:
		ScreenLoaded.NOTHING:
			# annulationd des mouvements du perso
			# get access to the player node (get_parent donne SceneManager)
			var player = Utils.get_player()
			if(!player.is_moving):
				player.set_physics_process(false)
				menu.visible = true
				pause_button.texture_normal = load("res://Assets/UI/returnbutton.png")
				update_GUI()
				screen_loaded = ScreenLoaded.JUST_MENU
		ScreenLoaded.JUST_MENU:
			# get access to the player node pour lui retablir les mouvements
			var player = Utils.get_player()
			player.set_physics_process(true)
			menu.visible = false
			pause_button.texture_normal = load("res://Assets/UI/Pause button.png")
			screen_loaded = ScreenLoaded.NOTHING

#---------------------------------------------------------------------------------------------------
# fait une maj des affichages du menu
func update_GUI():
	$Menu/Control/MissionLabel.text = PlayerInfo.missions[PlayerInfo.current_mission-1]
	$Menu/Control/ControlMenu/menu/VBoxContainer/header/name.text = PlayerInfo.player_name
	$Menu/Control/ControlMenu/menu/VBoxContainer/header/rank.text = "rang:"+str(PlayerInfo.player_rank)+"("+str(PlayerInfo.player_rank_xp)+"/"+str(PlayerInfo.calculate_next_level_rank())+")"
	$Menu/Control/ControlMenu/menu/VBoxContainer/header/money.text = str(PlayerInfo.money)
	$Menu/Control/ControlMenu/menu/VBoxContainer/header/charisme.text = "charisme:"+str(PlayerInfo.get_convinceAbility())
	$Menu/Control/ControlMenu/menu/VBoxContainer/header/levelLimit.text = "niveauMax:"+str(PlayerInfo.get_elemonLevelLimit())
	# translations of texts
	$Menu/Control/ControlMenu/menu/VBoxContainerLabel/elemenu/menu/recherche.text = tr("research_graph")
	$Menu/Control/ControlMenu/menu/VBoxContainerLabel/elemenu/menu/data.text = tr("data")
	$Menu/Control/ControlMenu/menu/VBoxContainerLabel/elemenu/menu/bag.text = tr("bag")
	$Menu/Control/ControlMenu/menu/VBoxContainerLabel/elemenu/menu/stats.text = tr("stats")
	$Menu/Control/ControlMenu/menu/VBoxContainerLabel/elemenu/menu/save.text = tr("save")
	$Menu/Control/ControlMenu/menu/VBoxContainerLabel/elemenu/menu/option.text = tr("options")
	if PlayerInfo.elemon11 != null:
		elemonLabel1.text = PlayerInfo.elemon11.get_name()+" ("+PlayerInfo.elemon11.get_string_rarity()+")"+"\npv: "+str(PlayerInfo.elemon11.current_pv)+"/"+str(PlayerInfo.elemon11.get_pvmax())+" lvl: "+str(PlayerInfo.elemon11.level)+" xp: "+str(PlayerInfo.elemon11.xp)
	if PlayerInfo.elemon12 != null:
		elemonLabel2.text = PlayerInfo.elemon12.get_name()+" ("+PlayerInfo.elemon12.get_string_rarity()+")"+"\npv: "+str(PlayerInfo.elemon12.current_pv)+"/"+str(PlayerInfo.elemon12.get_pvmax())+" lvl: "+str(PlayerInfo.elemon12.level)+" xp: "+str(PlayerInfo.elemon12.xp)
	if PlayerInfo.elemon13 != null:
		elemonLabel3.text = PlayerInfo.elemon13.get_name()+" ("+PlayerInfo.elemon13.get_string_rarity()+")"+"\npv: "+str(PlayerInfo.elemon13.current_pv)+"/"+str(PlayerInfo.elemon13.get_pvmax())+" lvl: "+str(PlayerInfo.elemon13.level)+" xp: "+str(PlayerInfo.elemon13.xp)
	if PlayerInfo.elemon14 != null:
		elemonLabel4.text = PlayerInfo.elemon14.get_name()+" ("+PlayerInfo.elemon14.get_string_rarity()+")"+"\npv: "+str(PlayerInfo.elemon14.current_pv)+"/"+str(PlayerInfo.elemon14.get_pvmax())+" lvl: "+str(PlayerInfo.elemon14.level)+" xp: "+str(PlayerInfo.elemon14.xp)
	if PlayerInfo.elemon15 != null:
		elemonLabel5.text = PlayerInfo.elemon15.get_name()+" ("+PlayerInfo.elemon15.get_string_rarity()+")"+"\npv: "+str(PlayerInfo.elemon15.current_pv)+"/"+str(PlayerInfo.elemon15.get_pvmax())+" lvl: "+str(PlayerInfo.elemon15.level)+" xp: "+str(PlayerInfo.elemon15.xp)
	var all_researcher = PlayerInfo.get_all_researcher()
	for i in range(0,3):
		if all_researcher[i]!=null:
			researcherLabels[i].text = all_researcher[i].get_name()+"  E: "+str(all_researcher[i].current_energy)+"/"+str(all_researcher[i].get_energyMax())+"\nERP: "+str(all_researcher[i].get_elemons_research_power())+"  TRP:"+str(all_researcher[i].get_types_research_power())

func _on_save_pressed():
	PlayerInfo.save_data()
	$Menu/Control/savePopup.popup_centered()


func _on_elemonButton_pressed(elemon_number):
	$ElemonInfos.first_load_elemonInfos(elemon_number)

func _on_researcherButton_pressed(researcher_number):
	$ResearcherInfos.load_page(researcher_number)

func _on_SideMissionsButton_pressed():
	$SideMissions.load_page()

func _on_recherche_pressed():
	$Research.load_page()

func _on_data_pressed():
	$Data.load_data()

func _on_bag_pressed():
	$PlayerBag.first_load(0)

func _on_option_pressed():
	$Options.load_page()

# fonction appelé quand pnj a fini de parlé mission 3
func choose_starter():
	$ChooseStarter.load_page()

func display_store_interface(treament_limit:int, gifts_limit:int):
	$StoreInterface.load_page(treament_limit, gifts_limit)

