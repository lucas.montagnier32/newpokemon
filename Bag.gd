extends Area2D

export var what_in:String # correspond a une clé dans un bag de PlayerInfo

export var id:int
var is_claimed:bool
var is_player_near:bool
var player

# Called when the node enters the scene tree for the first time.
func _ready():
	is_claimed = PlayerInfo.claimed_map_bags.has(self.id)
	if is_claimed: # destruction si déja récupérer
		self.queue_free()
	is_player_near = false
	player = Utils.get_player()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if !is_claimed && Input.is_action_just_pressed("a") && is_player_near:
		add_to_the_player_bag()
		self.is_claimed = true
		self.queue_free()


func add_to_the_player_bag():
	var text_to_display:String = "Argent +10, "
	if self.what_in in ["petit", "moyen", "bon", "grand", "génial", "superbe"]:
		text_to_display += self.what_in + " cadeau +1"
		PlayerInfo.bag_gifts[self.what_in] += 1
	elif "pv" in self.what_in || "pe" in self.what_in:
		text_to_display += self.what_in + "+1"
		PlayerInfo.bag_treatment[self.what_in] += 1
	elif "tech" in self.what_in:
		text_to_display += self.what_in + " obtenu"
		PlayerInfo.bag_capacites[self.what_in] += 1
	PlayerInfo.money += 10
	Utils.player_won_objects(text_to_display)
	PlayerInfo.claimed_map_bags.push_back(self.id)


func _on_bag_body_entered(_body):
	is_player_near = true


func _on_bag_body_exited(_body):
	is_player_near = false
