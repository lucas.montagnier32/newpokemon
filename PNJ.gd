extends KinematicBody2D

const TILE_SIZE = 16
const WALK_SPEED = 4.0

# id_pnj = 0 pour les pnj inutiles
# id_pnj < 0 pour les pnj de missions secondaires
# id_pnj > 0 pour les pnj de missions principales
export var id_pnj:int
# texte quand le pnj parle
export var text:Array
# direction initial du pnj
export var initial_direction:Vector2
export var is_mission_pnj:bool

onready var speek_label = $SpeekLabel
# variable pour les animations
onready var anim_tree = $AnimationTree
onready var anim_state = anim_tree.get("parameters/playback")

signal next_move_signal
signal mission1_finished
signal mission2_finished
signal mission3_finished
signal mission4_finished
signal moves_finished

var current_move:int
var current_text:int
var desired_position:Vector2
var initial_position:Vector2
var input_direction:Vector2

var is_ending_of_mission1:bool
var is_ending_of_mission2:bool
var is_ending_of_mission3:bool
var is_ending_of_mission4:bool
var mission3_fight_end:bool
var is_moving:bool
var number_of_moves:int
var percent_moving:float = 0.0
var player
var steps_moves_array:Array
var text_speed:float = 1.0
var walking_tile_number:int

# Called when the node enters the scene tree for the first time.
func _ready():
	is_moving = false
	speek_label.visible = false
	$Sprite.visible = true
	$TextureRect.visible = false
	if(is_mission_pnj && self.id_pnj!=4):
		anim_tree.active = true
	is_ending_of_mission1 = false
	is_ending_of_mission2 = false
	is_ending_of_mission3 = false
	is_ending_of_mission4 = false
	mission3_fight_end = false
	anim_tree.set("parameters/Idle/blend_position", input_direction)
	anim_tree.set("parameters/Walk/blend_position", input_direction)
	anim_tree.set("parameters/Turn/blend_position", input_direction)
	current_move = 0
	current_text = 0
	# init de la direction
	match initial_direction:
		Vector2(-1,0):
			$Sprite.frame = 7
			$Sprite.flip_h = false
		Vector2(1,0):
			$Sprite.frame = 7
			$Sprite.flip_h = true
		Vector2(0,-1):
			$Sprite.frame = 4
		Vector2(0,1):
			$Sprite.frame = 1
	player = Utils.get_player()
	# connection des signaux
	player.connect("player_want_to_talk_signal", self, "talk_to_pnj")
	# warning-ignore:return_value_discarded
	self.connect("next_move_signal",self,"next_move")


#---------------------------------------------------------------------------------------------------
# fonction appelé depuis Player pour faire bouger le pnj (en nombre de tuiles)
func move(moves_array:Array):
	current_move = 0
	steps_moves_array = moves_array
	number_of_moves = moves_array.size()
	next_move()

func next_move():
	one_move(steps_moves_array[current_move])

func one_move(move:Vector2):
	initial_position = position
	desired_position = initial_position + move*TILE_SIZE
	if(move.x>0):
		input_direction = Vector2(1,0)
		walking_tile_number = int(abs(move.x))
	elif(move.x<0):
		input_direction = Vector2(-1,0)
		walking_tile_number = int(abs(move.x))
	elif(move.y>0):
		input_direction = Vector2(0,1)
		walking_tile_number = int(abs(move.y))
	elif(move.y<0):
		input_direction = Vector2(0,-1)
		walking_tile_number = int(abs(move.y))
	is_moving = true
	anim_tree.set("parameters/Idle/blend_position", input_direction)
	anim_tree.set("parameters/Walk/blend_position", input_direction)
	anim_tree.set("parameters/Turn/blend_position", input_direction)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if(is_mission_pnj):
		if (input_direction != Vector2.ZERO):
			# changement d'état dans l'arbre d'animation
			anim_state.travel("Walk")
			percent_moving += WALK_SPEED * delta
			if (percent_moving >= walking_tile_number): # fin du mouvement
				# pour être sur qu'il arrive bien a la bonne position (position finale)
				position = initial_position + (TILE_SIZE * input_direction * walking_tile_number)
				percent_moving = 0.0
				input_direction = Vector2(0,0)
				if(current_move+1<number_of_moves):
					current_move += 1
					emit_signal("next_move_signal")
				elif(current_move+1==number_of_moves): # fin des moves
					emit_signal("moves_finished")
			else:
				# interpolation de la position selon le pourcentage
				position = initial_position + (TILE_SIZE * input_direction * percent_moving)
		else: # sinon ne bouge pas
			anim_state.travel("Idle")
			is_moving = false
	# INPUT pour passer le texte
	if current_text<self.text.size() && current_text!=0 && Input.is_action_just_pressed("ok") && Vector2(abs(player.position.x-self.position.x),abs(player.position.y-self.position.y))<Vector2(TILE_SIZE, TILE_SIZE):
		_on_Timer_timeout()


#---------------------------------------------------------------------------------------------------
# PNJ TALKING

func talk_to_pnj():
	if self.id_pnj==-1: # mission secondaire -1
		if PlayerInfo.bag_objects["collier"]==1: # mission finit
			self.text.clear()
			self.text.push_back("Tu a retrouvé le collier, merci !")
			PlayerInfo.side_missions[0]=2
			PlayerInfo.add_xp_rank(5)
			PlayerInfo.money+=30
			Utils.player_won_objects("XP +5 Argent +30")
		else:
			PlayerInfo.side_missions[0]=1
	# MISSION 3
	if(self.id_pnj==4 && PlayerInfo.current_mission==3):
		print("start mission 3")
		player.stop_input = true
	
	player.stop_input_for_pnj = true
	# changement de la direction du pnj
	var player_position = player.position
	# le joueur est a gauche du pnj
	if(player_position.x==position.x-16 and player_position.y==position.y):
		$Sprite.frame=7
		$Sprite.flip_h=false
		show_text()
	# le joueur est a droite du pnj
	elif(player_position.x==position.x+16 and player_position.y==position.y):
		$Sprite.frame=7
		$Sprite.flip_h=true
		show_text()
	# le joueur est au dessus du pnj
	elif(player_position.x==position.x and player_position.y==position.y-16):
		$Sprite.frame=4
		show_text()
	# le joueur est en dessous du pnj
	elif(player_position.x==position.x and player_position.y==position.y+16):
		$Sprite.frame=1
		# REPETITION des show_text() normal sinon tous les pnj afficherait leur text
		show_text()


func _on_PNJ_moves_finished():
	if(is_ending_of_mission1): # après que les pnj soient partis
		emit_signal("mission1_finished")
		player.stop_input = false
		PlayerInfo.current_mission += 1
		print("mission 1 finished")
	if(is_ending_of_mission2):
		emit_signal("mission2_finished")
		player.stop_input = false
		PlayerInfo.current_mission +=1
		print("mission 2 finished")
	if(is_ending_of_mission3):
		emit_signal("mission3_finished")
		player.stop_input = false
		player.stop_input_for_pnj = false
		PlayerInfo.current_mission +=1
		print("mission 3 finished")
	if(is_ending_of_mission4):
		emit_signal("mission4_finished")
		player.stop_input = false
		player.stop_input_for_pnj = false
		PlayerInfo.current_mission +=1
		print("mission 4 finished")
	
	show_text()


func show_text():
	if(current_text<self.text.size()):
		$TextureRect.visible = true
		speek_label.text = self.text[current_text]
		speek_label.visible = true
		$Timer.wait_time = 0.5+self.text[current_text].length()/(15.0*text_speed)
		$Timer.start()
	else: # fin des textes
		speek_label.text = ""
		speek_label.visible = false
		$TextureRect.visible = false
		$Timer.stop()
		player.stop_input_for_pnj = false
		if(!is_mission_pnj):
			current_text = 0
		
		if(self.id_pnj==1 && PlayerInfo.current_mission==1): # fin mission 1
			self.move([Vector2(0,6)])
			Utils.get_pnj_from_id(2).move([Vector2(0,6)])
			is_ending_of_mission1 = true
		if(self.id_pnj==3 && PlayerInfo.current_mission==2): # fin mission 2
			self.move([Vector2(0,2), Vector2(-10,0)])
			is_ending_of_mission2 = true
		if(self.id_pnj==4 && PlayerInfo.current_mission==3 && !mission3_fight_end): # fin texte mission 3
			Utils.get_GUI().choose_starter()
		if(mission3_fight_end):
			self.anim_tree.active = true
			self.move([Vector2(-1,0), Vector2(0,3), Vector2(1,0), Vector2(0,5)])
			is_ending_of_mission3 = true
		if(self.id_pnj==5 && PlayerInfo.current_mission==4):
			self.move([Vector2(10,0)])
			is_ending_of_mission4 = true


func _on_Timer_timeout():
	current_text += 1
	show_text()

# fonction appelé depuis SceneManager après le premier combat mission 3
func end_of_mission3():
	self.text.clear()
	self.text.push_back("Tu te bats déjà bien")
	self.text.push_back("Il y a une aire de soins au nord d'ici pour le soigner")
	self.text.push_back("On se reverra bientôt")
	self.talk_to_pnj()
	mission3_fight_end = true


func _on_Player_hearthquake_finished():
	if self.id_pnj==5 && PlayerInfo.current_mission==4:
		player.stop_input = true
		self.position.y = player.position.y
		self.move([Vector2(-10,0)])
