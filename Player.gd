# INFO: 
# BlockingRayCast2D est pour bloquer le joueur avec des collision
# LedgeRayCast2D est pour laisser passer par dessus le ledge
# DoorRayCast2D est pour la porte des maisons

extends KinematicBody2D

# signals pour le grass overlay
signal player_moving_signal
signal player_stopped_signal

# signaux pour le joueur qui utilise une porte
signal player_entering_door_signal
signal player_entered_door_signal

# signal du joueur proche d'un pnj
signal player_want_to_talk_signal

signal hearthquake_finished

# load la scene dans une variable pour y avoir accès
const LandingDustEffect = preload("res://LandingDustEffect.tscn")

# export pour changer la variable pendant l'execution
export var walk_speed = 4.0
export var jump_speed = 4.0
export var finding_pokemon_rate = 0.3
export var is_in_grass:bool = false

# taille constante d'une tuile
const TILE_SIZE = 16

# variable pour les animations
onready var anim_tree = $AnimationTree
onready var anim_state = anim_tree.get("parameters/playback")

# variables des rays pour les collisions
onready var ray = $BlockingRayCast2D
onready var ledge_ray = $LedgeRayCast2D
onready var door_ray = $DoorRayCastD2
onready var pnj_ray = $PNJRayCast
onready var stair_up_ray = $StairUpRayCast

# pour l'ombre quand il saute par desss un ledge
onready var shadow = $Shadow

# vrai si le joueur suate par dessus une ledge
var jumping_over_ledge:bool = false

# vrai si le perso peut tourner sur lui meme
var ENABLE_TURNING = true

# var pour les changement d'animations
enum PlayerState { IDLE, TURNING, WALKING }
enum FacingDirection { LEFT, RIGHT, UP, DOWN }

var begin_fight:bool = false # sert a lancer le combat après dialogue

# état du perso et vers ou il fait face
var player_state = PlayerState.IDLE
var facing_direction = FacingDirection.DOWN

# position du perso avant qu'il bouge sur une autre tuile
var initial_position = Vector2(0,0)
# dans quelle direction le perso bouge
var input_direction = Vector2(0,1)
# vrai si le perso bouge
var is_moving = false
# bloque l'input de direction si le perso est entré dans une porte
var stop_input:bool = false
var stop_input_for_pnj:bool = false
# pourcentage entre 0 et 1 du mouvement du personnage entre 2 tuiles
var percent_moving = 0.0
# pour smooth l'augmentation du smooth camera avec escaliers
var smooth_increasing_smooth = false

#---------------------------------------------------------------------------------------------------
# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	$Sprite.visible = true
	# active l'arbre d'animation
	anim_tree.active = true
	# position donne la position actuelle du perso
	initial_position = position
	shadow.visible = false
	#init la direction du player
	anim_tree.set("parameters/Idle/blend_position", input_direction)
	anim_tree.set("parameters/Walk/blend_position", input_direction)
	anim_tree.set("parameters/Turn/blend_position", input_direction)

#---------------------------------------------------------------------------------------------------
# set the spawn location and direction of the player
func set_spawn(location:Vector2, direction:Vector2):
	# set la direction du player
	anim_tree.set("parameters/Idle/blend_position", direction)
	anim_tree.set("parameters/Walk/blend_position", direction)
	anim_tree.set("parameters/Turn/blend_position", direction)
	position = location # set position du player

#---------------------------------------------------------------------------------------------------
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	if is_earthquake:
		earthquake(2)
	if smooth_increasing_smooth && $Timer.is_stopped():
		$Timer.wait_time = 0.1
		$Timer.start()
	if (player_state == PlayerState.TURNING) or stop_input:
		return
	elif (!is_moving): # le joueur choisi une direction si il ne bouge pas
		processPlayerInput()
	elif (input_direction != Vector2.ZERO): # le joueur bouge si il a choisi une direction
		# changement d'état dans l'arbre d'animation
		anim_state.travel("Walk")
		move(delta)
	else: # sinon le joueur ne bouge pas
		anim_state.travel("Idle")
		is_moving = false


var is_earthquake:bool = false
var to_right:bool = true
var earthquake_progress:float = 495.0
onready var camera:Camera2D = $Camera2D

func earthquake(_force:int):
	self.stop_input = true
	anim_state.travel("Idle")
	if to_right:
		camera.offset.x += earthquake_progress/100.0
	else:
		camera.offset.x -= earthquake_progress/100.0
	if camera.offset.x >= 5:
		to_right = false
	if camera.offset.x <= -5:
		to_right = true
	earthquake_progress -= 1
	if earthquake_progress <= 100:
		is_earthquake = false
		camera.offset.x = 0
		self.stop_input = false
		emit_signal("hearthquake_finished")


#---------------------------------------------------------------------------------------------------
# test les entrées clavier et lance les animations correspondantes
func processPlayerInput():
	# change la direction du ray pour les pnj
	match facing_direction:
		FacingDirection.DOWN:
			pnj_ray.cast_to = Vector2(0.0,12.0)
		FacingDirection.LEFT:
			pnj_ray.cast_to = Vector2(-12.0,0.0)
		FacingDirection.RIGHT:
			pnj_ray.cast_to = Vector2(12.0,0.0)
		FacingDirection.UP:
			pnj_ray.cast_to = Vector2(0.0,-12.0)
	pnj_ray.force_raycast_update()
	# le joueur se retrouve en face d'un pnj
	if(pnj_ray.is_colliding() and Input.is_action_just_pressed("a")):
		anim_state.travel("Idle")
		emit_signal("player_want_to_talk_signal")
	
	# empeche les mouvement quand il parle a un pnj
	if(stop_input_for_pnj):
		anim_state.travel("Idle")
		return
	
	if (input_direction.y==0):
		# vaut -1 si gauche, 0 si aucun ou les deux et 1 si droite
		input_direction.x = int(Input.is_action_pressed("ui_right")) - int(Input.is_action_pressed("ui_left"))
	if (input_direction.x==0):
		input_direction.y = int(Input.is_action_pressed("ui_down")) - int(Input.is_action_pressed("ui_up"))
	# si le joueur bouge
	if (input_direction != Vector2.ZERO):
		# met l'animation correspondant a la direction
		anim_tree.set("parameters/Idle/blend_position", input_direction)
		anim_tree.set("parameters/Walk/blend_position", input_direction)
		anim_tree.set("parameters/Turn/blend_position", input_direction)
		
		if (needToTurn() and ENABLE_TURNING):
			player_state = PlayerState.TURNING
			anim_state.travel("Turn")
		else:
			initial_position = position
			is_moving = true
	else:
		anim_state.travel("Idle")

#---------------------------------------------------------------------------------------------------
# test si le perso doit tourner ou bouger
func needToTurn():
	var new_facing_dir
	if (input_direction.x<0):
		new_facing_dir = FacingDirection.LEFT
	elif (input_direction.x>0):
		new_facing_dir = FacingDirection.RIGHT
	elif (input_direction.y<0):
		new_facing_dir = FacingDirection.UP
	elif (input_direction.y>0):
		new_facing_dir = FacingDirection.DOWN
	
	if (facing_direction != new_facing_dir):
		facing_direction = new_facing_dir # actualisation de la facing_direction
		return true
	facing_direction = new_facing_dir # actualisation de la facing_direction
	return false

#---------------------------------------------------------------------------------------------------
# fonction appelé a la fin d'une animation de turning
func turnFinished():
	player_state = PlayerState.IDLE

#---------------------------------------------------------------------------------------------------
# fonction appelé a la fin de l'animation "disappear"
func enteredDoor():
	emit_signal("player_entered_door_signal")

#---------------------------------------------------------------------------------------------------
# calcul de la position du perso en mouvement et des rays pour les collisions
func move(delta):
	# 'delta' is the elapsed time since the previous frame.
	# donne le vecteur2 de la prchaine tuile a partir de la tuile actuelle
	var desired_step:Vector2 = input_direction * TILE_SIZE /2
	# change la direction du ray de blocage
	ray.cast_to = desired_step
	ray.force_raycast_update()
	# change la direction du ray de collision avec les ledges
	ledge_ray.cast_to = desired_step
	ledge_ray.force_raycast_update()
	#  change la direction du ray pour les portes
	door_ray.cast_to = desired_step
	door_ray.force_raycast_update()
	
	stair_up_ray.cast_to = desired_step
	stair_up_ray.force_raycast_update()

	if (door_ray.is_colliding()):
		if(percent_moving==0.0):
			emit_signal("player_entering_door_signal")
		percent_moving += walk_speed * delta
		if(percent_moving>=1.0):
			position = initial_position + (TILE_SIZE * input_direction)
			percent_moving = 0.0
			is_moving = false
			stop_input = true
			# Player disparait (changement de scene)
			$AnimationPlayer.play("Disappear")
			# nettoyage de la caméra
			camera.clear_current()
		else:
			position = initial_position + (TILE_SIZE * input_direction * percent_moving)
		
	# percent moving est le pourcentage d'avancement du perso entre deux tuiles
	# condition si il veut sauter ou si il saute deja par dessus un ledge
	elif (ledge_ray.is_colliding() && input_direction == Vector2(0,1)) or jumping_over_ledge:
		percent_moving += jump_speed * delta
		if (percent_moving >= 2.0): # si le joueur a parcourus 2 tuiles
			# pour être sur qu'il arrive bien a la bonne position (position finale)
			position = initial_position + (TILE_SIZE * 2 * input_direction)
			percent_moving = 0.0 # remise a 0
			is_moving = false
			jumping_over_ledge = false
			shadow.visible = false
			# dust landing effect
			var dust_effect = LandingDustEffect.instance()
			dust_effect.position = position
			get_tree().current_scene.add_child(dust_effect)
			
		else:
			shadow.visible = true
			jumping_over_ledge = true
			# fonction quadratique pour le déplacement pendant le saut
			var input = input_direction.y * TILE_SIZE * percent_moving
			position.y = initial_position.y + (-0.96 - 0.53*input + 0.05*pow(input,2))
			
	elif (!ray.is_colliding() || stair_up_ray.is_colliding()): # test si le ray de blocage est en collision
		if stair_up_ray.is_colliding():
			if input_direction == Vector2(0,1): # descend escalier
				self.walk_speed = 6
				camera.smoothing_speed = 4
				camera.smoothing_enabled = true
			elif input_direction == Vector2(0,-1): # monte escalier
				self.walk_speed = 3
				camera.smoothing_speed = 4
				camera.smoothing_enabled = true
		else:
			stair_up_ray.cast_to = self.position
			stair_up_ray.force_raycast_update()
			if self.walk_speed!=4 && !stair_up_ray.is_colliding(): # si le player n'est plus dans un escalier
				smooth_increasing_smooth = true
				self.walk_speed = 4
		if(percent_moving==0):
			emit_signal("player_moving_signal")
		percent_moving += walk_speed * delta
		if (percent_moving >= 1): # fin du mouvement
			# pour être sur qu'il arrive bien a la bonne position (position finale)
			position = initial_position + (TILE_SIZE * input_direction)
			percent_moving = 0.0
			emit_signal("player_stopped_signal")
			is_moving = false
			
			if(is_in_grass && PlayerInfo.nb_elemons_f>0):
				var random_float = randf()
				# trouvé un pokemon
				if(random_float<finding_pokemon_rate):
					stop_input = true
					Utils.get_scene_manager().transition_to_battle($Sprite.frame,$Sprite.flip_h)
		else:
			# interpolation de la position selon le pourcentage
			position = initial_position + (TILE_SIZE * input_direction * percent_moving)
	else:
		is_moving = false


func _on_Timer_timeout(): # pour les smooth camera escaliers
	if camera.smoothing_speed < 20:
		camera.smoothing_speed += 1
	else:
		smooth_increasing_smooth = false
		camera.smoothing_enabled = false
		$Timer.stop()
