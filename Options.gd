extends Control



# Called when the node enters the scene tree for the first time.
func _ready():
	self.visible = false
	$VBoxContainer/lang.text = "Language: "+TranslationServer.get_locale_name(TranslationServer.get_locale())

func load_page():
	self.visible = true

#---------------------------------------------------------------------------------------------------
# button action
func _on_return_pressed():
	Utils.get_GUI().update_GUI()
	self.visible = false

# suppression de la sauvegarde
func _on_deleteButton_pressed():
	$ConfirmationDialog.popup_centered()

# suppression de la sauvegarde
func _on_ConfirmationDialog_confirmed():
	PlayerInfo.remove_save_file()
	get_tree().quit()


func _on_lang_pressed(num_lang):
	# remettre a zero tout les autres
	var lang_buttons:Array = $VBoxContainer/HBoxContainer.get_children()
	for i in range(0, $VBoxContainer/HBoxContainer.get_child_count()):
		if i!=num_lang-1:
			lang_buttons[i].pressed = false
	# and change the game language
	match num_lang:
		1:
			TranslationServer.set_locale("en")
		2:
			TranslationServer.set_locale("es")
		3:
			TranslationServer.set_locale("fr")
	$VBoxContainer/lang.text = "Language: "+TranslationServer.get_locale_name(TranslationServer.get_locale())
