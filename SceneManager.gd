extends Node2D

# store the string value of the next scene
var next_scene = null

signal elemons_loaded_signal

# save le spawn location and direction
var player_location = Vector2(0,0)
var player_direction = Vector2(0,0)

# pour les équipes des dresseur ennemi
var ennemies_vector_array:PoolVector2Array
var id_trainer:int
var is_player_won:bool
var trainer_elemons_power:int

# transition vers une nouvelle scene, vers la party screen, vers le menu depuis la party screen
enum TransitionType { NEW_SCENE, PARTY_SCREEN, MENU_ONLY, BATTLE_SCREEN, BATTLE_TRAINER_SCREEN, EXIT_BATTLE, EXIT_TRAINER_BATTLE }
var transition_type = TransitionType.NEW_SCENE

# pour save la scene dans laquelle le joueur se trouve
enum Scene { PLAYER_HOME_FLOOR1 , PLAYER_HOME_FLOOR2 , HOUSE1_FLOOR1 , OAKSLAB , TOWN }
var current_scene = "res://PlayerHomeFloor2.tscn"

#---------------------------------------------------------------------------------------------------
# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	$ScreenTransition/ColorRect.visible = true
	$ScreenTransition/ColorRect.color.a=0.0
	# quand un save & load est modifié, faut mettre un false && PlayerInfo.exist_load() 
	# et faire un save dans le jeu pour remettre a zero la sauvegarde
	if(PlayerInfo.exist_load()):
		$CurrentScene.get_child(0).queue_free() # suppression de l'ancienne scene
		# load la scene correspondante lors de la sauvegarde
		$CurrentScene.add_child(load(PlayerInfo.current_scene).instance())
		Utils.get_player().set_spawn(PlayerInfo.position, Vector2(0,1))
	else:
		# si pas de sauvegarde
		# on supprime l'ancienne scene et load playerhomefloor2 pour regler le probleme du landing dust effect
		$CurrentScene.get_child(0).queue_free() # suppression de l'ancienne scene
		$GUI/Pause/PauseButton.visible = false
		$CurrentScene.add_child(load("res://Intro.tscn").instance()) # ajout de la nouvelle scene
		PlayerInfo.current_scene = "res://PlayerHomeFloor2.tscn"

#---------------------------------------------------------------------------------------------------
# lancement de la party screen
func transition_to_party_screen():
	transition_type = TransitionType.PARTY_SCREEN
	$ScreenTransition/AnimationPlayer.play("FadeToBlack")

#---------------------------------------------------------------------------------------------------
# sortie de la scene party screen
func transition_exit_party_screen():
	transition_type = TransitionType.MENU_ONLY
	$ScreenTransition/AnimationPlayer.play("FadeToBlack")

#---------------------------------------------------------------------------------------------------
# lancement d'une nouvelle scene
func transitionToScene(new_scene:String, spawn_location, spawn_direction):
	next_scene = new_scene
	# save le spawn location and direction
	player_location=spawn_location
	player_direction=spawn_direction
	transition_type = TransitionType.NEW_SCENE
	$ScreenTransition/AnimationPlayer.play("FadeToBlack")

#---------------------------------------------------------------------------------------------------
# lancement d'une nouvelle bataille
func transition_to_battle(player_frame,player_flip_h):
	next_scene = "res://BattleScreen.tscn"
	# recharge la location save avant de lancer la battle screen
	var player = Utils.get_player()
	player_location = player.position
	# direction du perso 
	if(player_frame <= 3):
		player_direction = Vector2(0,1)
	elif(player_frame <= 5):
		player_direction = Vector2(0,-1)
	else:
		if(player_flip_h):
			player_direction = Vector2(1,0)
		else:
			player_direction = Vector2(-1,0)
	transition_type = TransitionType.BATTLE_SCREEN
	$ScreenTransition/AnimationPlayer.play("FadeToBlack")


#---------------------------------------------------------------------------------------------------
# lancement d'une nouvelle bataille avec un dresseur
func transition_to_trainer_battle(player_frame,player_flip_h, id_and_lvl_elemons, id_traine, power:int):
	next_scene = "res://BattleScreen.tscn"
	# recharge la location save avant de lancer la battle screen
	var player = Utils.get_player()
	player_location = player.position
	ennemies_vector_array = id_and_lvl_elemons
	self.id_trainer = id_traine
	self.trainer_elemons_power = power
	# direction du perso 
	if(player_frame <= 3):
		player_direction = Vector2(0,1)
	elif(player_frame <= 5):
		player_direction = Vector2(0,-1)
	else:
		if(player_flip_h):
			player_direction = Vector2(1,0)
		else:
			player_direction = Vector2(-1,0)
	transition_type = TransitionType.BATTLE_TRAINER_SCREEN
	$ScreenTransition/AnimationPlayer.play("FadeToBlack")


#---------------------------------------------------------------------------------------------------
# lancement d'une nouvelle bataille
func transition_exit_battle(new_scene:String, is_player_win:bool, is_fleeing:bool):
	next_scene = new_scene
	transition_type = TransitionType.EXIT_BATTLE
	is_player_won = is_player_win || is_fleeing
	$ScreenTransition/AnimationPlayer.play("FadeToBlack")


#---------------------------------------------------------------------------------------------------
# lancement d'une nouvelle bataille
func transition_exit_trainer_battle(new_scene:String, is_player_win:bool):
	next_scene = new_scene
	transition_type = TransitionType.EXIT_TRAINER_BATTLE
	is_player_won = is_player_win
	$ScreenTransition/AnimationPlayer.play("FadeToBlack")


#---------------------------------------------------------------------------------------------------
# suppresion de l'ancienne scene et apparition de la nouvelle scene
# sera appelé quand l'animation FadeToBlack est fini
func finishedFading():
	match transition_type:
		TransitionType.NEW_SCENE:
			$CurrentScene.get_child(0).queue_free() # suppression de l'ancienne scene
			$CurrentScene.add_child(load(next_scene).instance()) # ajout de la nouvelle scene
			
			if(next_scene=="res://PlayerHomeFloor2.tscn"):# après l'intro besoin de reafficher le bouton
				$GUI/Pause/PauseButton.visible = true
			
			# recuperation du noeud Player de la scene la derniere ajouter (.back) dans CurrentScene
			var player = Utils.get_player()
			# set the spawn of the player
			player.set_spawn(player_location, player_direction)
			
			# changement de la scene courante
			PlayerInfo.current_scene = next_scene
		
		# on peut recup menu avec $Menu car it's a child
		TransitionType.PARTY_SCREEN:
			$GUI.load_party_screen()
		
		TransitionType.MENU_ONLY:
			$GUI.unload_party_screen()
		
		TransitionType.BATTLE_SCREEN:
			$CurrentScene.add_child(load(next_scene).instance()) # ajout de la nouvelle scene
			# Choisir un Elemon aléatoire en fonction du nombre d'Elemon dans la current_scene
			var old_current_scene = $CurrentScene.get_child(0)
			#choix parmis les elemons dans lascene dans laquelle le joueur se trouvait
			var randint = randi() % old_current_scene.fightermons.size()
			var id_elemon = old_current_scene.fightermons[randint][0] # recup de l'id
			# init de l'ennemi
			var elemon_ennemi = Fightermon.Fightermon_class.new() # création nouveau elemon
			var randint_lvl = (randi() % 4) + old_current_scene.fightermons[randint][1]
			elemon_ennemi.set_stats_from_id_and_level(id_elemon, randint_lvl, Fightermon.rarity_enum.NORMAL)# init des stats de l'elemon
			elemon_ennemi.set_capa_from_id(0,0)
			
			$CurrentScene.get_child(1).ennemies.insert(0,elemon_ennemi) # passage au BattleScreen
			$CurrentScene.get_child(1).is_trainer_battle = false
			
			old_current_scene.queue_free() # suppression de l'ancienne scene
			$GUI/Pause/PauseButton.visible = false
			emit_signal("elemons_loaded_signal")
		
		TransitionType.BATTLE_TRAINER_SCREEN:
			$CurrentScene.add_child(load(next_scene).instance()) # ajout de la nouvelle scene
			
			var old_current_scene = $CurrentScene.get_child(0)
			# Set les elemons du trainer ennemi
			var ennemies_array:Array = []
			var id_elemon_ennemi
			for i in range(0,ennemies_vector_array.size()):
				id_elemon_ennemi = ennemies_vector_array[i].x
				ennemies_array.insert(i,Fightermon.Fightermon_class.new())
				ennemies_array[i].set_stats_from_id_and_level(id_elemon_ennemi, ennemies_vector_array[i].y, self.trainer_elemons_power)
				for j in range(0,Fightermon.capa_de_base_ennemi_array[id_elemon_ennemi].size()):
					ennemies_array[i].set_capa_from_id(Fightermon.capa_de_base_ennemi_array[id_elemon_ennemi][j],0)
			
			$CurrentScene.get_child(1).ennemies = ennemies_array # passage au BattleScreen
			$CurrentScene.get_child(1).is_trainer_battle = true
			
			old_current_scene.queue_free() # suppression de l'ancienne scene
			$GUI/Pause/PauseButton.visible = false
			emit_signal("elemons_loaded_signal")
		
		TransitionType.EXIT_BATTLE:
			$CurrentScene.get_child(0).queue_free() # suppression de l'ancienne scene
			$CurrentScene.add_child(load(next_scene).instance()) # ajout de la nouvelle scene
			$GUI/Pause/PauseButton.visible = true
			
			# recuperation du noeud Player de la scene la derniere ajouter (.back) dans CurrentScene
			var player = Utils.get_player()
			if(is_player_won):
				player.is_in_grass = true
				# set the spawn of the player
				player.set_spawn(player_location, player_direction)
			else:
				no_more_ally(player)
		
		TransitionType.EXIT_TRAINER_BATTLE:
			$CurrentScene.get_child(0).queue_free() # suppression de l'ancienne scene
			$CurrentScene.add_child(load(next_scene).instance()) # ajout de la nouvelle scene
			
			$GUI/Pause/PauseButton.visible = true
			
			# recuperation du noeud Player de la scene la derniere ajouter (.back) dans CurrentScene
			var player = Utils.get_player()
			# mettre le trainer comme battu si le joueur a gagné
			if(is_player_won):
				if self.id_trainer==5: # misision secondaire -1 fini
					PlayerInfo.bag_objects["collier"]=1
				if(self.id_trainer!=-1): # si pas le trainer de la mission 3
					Utils.get_trainer_from_id(id_trainer).is_defeated = true
					if (PlayerInfo.defeatedTrainerId.has(id_trainer)):
						print("Le dresseur d'id "+str(id_trainer)+" est déja battu")
					else:
						PlayerInfo.defeatedTrainerId.push_back(id_trainer)
						print(PlayerInfo.defeatedTrainerId)
				
				player.is_in_grass = false
				# set the spawn of the player
				player.set_spawn(player_location, player_direction)
				
				# après combat mission 3 (starter fight)
				if(self.id_trainer==-1):
					Utils.get_pnj_from_id(4).end_of_mission3()
			else:
				no_more_ally(player)
		
	$ScreenTransition/AnimationPlayer.play("FadeToNormal")


func no_more_ally(player):
	$CurrentScene.get_child(0).queue_free() # suppression de l'ancienne scene
	$CurrentScene.add_child(load("res://Town.tscn").instance()) # ajout de la nouvelle scene
	player.is_in_grass = false
	# set the spawn of the player
	player.set_spawn(Vector2(0,0), Vector2(0,0))
	PlayerInfo.heal_all_max()
