extends Control

const button_normal = preload("res://Assets/UI/button_normal.png")
const button_hover = preload("res://Assets/UI/button_hover.png")
const button_pressed = preload("res://Assets/UI/button_touch.png")

onready var names = $ScrollContainer/content/contentText/names
onready var contentButtonNode = $ScrollContainer/content/contentButton

var elemons_array_size:int

# Called when the node enters the scene tree for the first time.
func _ready():
	elemons_array_size = Fightermon.figthermons_array.size()
	self.visible = false


func load_data():
	self.visible = true
	$ScrollContainer/content.rect_min_size = Vector2(160,elemons_array_size*26)
	names.text = ""
	var button:Control
	for n in contentButtonNode.get_children(): # suppressions des anciens boutons
		contentButtonNode.remove_child(n)
		n.queue_free()
	for i in range(0, elemons_array_size):
		names.text += str(i)+":"+Fightermon.figthermons_array[i][0]+"\n"
		
		button = TextureButton.new()
		button.expand = true
		button.texture_normal = button_normal
		button.texture_hover = button_hover
		button.texture_pressed = button_pressed
		button.rect_min_size = Vector2(140,18)
		button.mouse_default_cursor_shape = Control.CURSOR_POINTING_HAND
		# warning-ignore:return_value_discarded
		button.connect("pressed", self, "_on_elemonButton_pressed", [ i ])
		contentButtonNode.add_child(button)


#---------------------------------------------------------------------------------------------------
# button action
func _on_return_pressed():
	self.visible = false

func _on_elemonButton_pressed(id):
	var infos_elemon = Fightermon.figthermons_array[id]
	$infos/up.text = infos_elemon[0]+"\nType: "+infos_elemon[1]+" "+infos_elemon[2]
	$infos/values/name.text = "PVmax: \nAtt: \nDef: "
	$infos/values/value.text = str(infos_elemon[3])+"\n"+str(infos_elemon[5])+"\n"+str(infos_elemon[6])
	$infos/values/name2.text = "PEmax: \nAttSpe: \nDefSpe: "
	$infos/values/value2.text = str(infos_elemon[4])+"\n"+str(infos_elemon[7])+"\n"+str(infos_elemon[8])
	$infos/down.text = "Vitesse: "+str(infos_elemon[9])+"\n Type d'évolution: "+infos_elemon[10]
	$infos/elemonImage.texture = load(infos_elemon[11])
	$infos/elemonImage.region_enabled = true
	$infos/elemonImage.region_rect.position = Vector2(0,35)
	$infos/elemonImage.region_rect.size = Vector2(60,60)
