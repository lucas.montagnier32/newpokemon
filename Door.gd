extends Area2D

# definition du chemin vers la nouvelle scene
export(String, FILE) var next_scene_path = ""
export(bool) var is_invisible = false

# spawn location and direction
export(Vector2) var spawn_location = Vector2(0,0)
export(Vector2) var spawn_direction = Vector2(0,0)

# get access to the sprite and the animation player
onready var sprite = $Sprite
onready var anim_player = $AnimationPlayer

# vrai si le joueur est dans la porte, permet de lancer l'animation uniquement la ou le joueur entre
var player_entered = false

# Called when the node enters the scene tree for the first time.
func _ready():
	if (is_invisible): # rendu invisible quand la variable est initialisé a vrai
		$Sprite.texture = null
	sprite.visible = false
	# recuperation du noeud Player de la scene la derniere ajouter (.back) dans CurrentScene
	var player = Utils.get_player()
	# connection des signaux
	player.connect("player_entering_door_signal", self, "enterDoor")
	player.connect("player_entered_door_signal", self, "closeDoor")

#---------------------------------------------------------------------------------------------------
# fonction appelé quand le joueur entre dans la porte
func enterDoor():
	if (player_entered):
		anim_player.play("OpenDoor")

#---------------------------------------------------------------------------------------------------
# fonction appelé quand le joueur est entré dans la porte
func closeDoor():
	if (player_entered):
		anim_player.play("CloseDoor")

#---------------------------------------------------------------------------------------------------
# fonction appelé pour changer de scene
func doorClosed():
	if (player_entered):
		Utils.get_scene_manager().transitionToScene(next_scene_path, spawn_location, spawn_direction)

#---------------------------------------------------------------------------------------------------
func _on_Door_body_entered(_body):
	player_entered = true

#---------------------------------------------------------------------------------------------------
func _on_Door_body_exited(_body):
	player_entered = false



# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
