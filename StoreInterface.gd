extends Control

const pv_prices = [7,14,28,56,112]
const pe_prices = [4,7,14,28,56]
const gifts_prices = [6,14,22,30,38,46]

var button_theme = preload("res://Assets/new_theme.tres")
var gifts:Array
var player_have_enough_money:bool
var purshased_num:int
var sum:int
var treaments:Array
var wanted_number:int = 1
var what_is_purchased:String

# Called when the node enters the scene tree for the first time.
func _ready():
	self.visible = false
	self.gifts = PlayerInfo.bag_gifts.keys()
	self.treaments = PlayerInfo.bag_treatment.keys()
	$HBoxContainer/money.text = "Vous avez "+str(PlayerInfo.money)+"€"


func load_page(treament_limit:int, gifts_limit:int):
	self.visible = true
	for n in $objectsButtons/gifts.get_children(): # suppressions des anciens boutons
		$objectsButtons/gifts.remove_child(n)
		n.queue_free()
	for n in $objectsButtons/pe.get_children(): # suppressions des anciens boutons
		$objectsButtons/pe.remove_child(n)
		n.queue_free()
	for n in $objectsButtons/pv.get_children(): # suppressions des anciens boutons
		$objectsButtons/pv.remove_child(n)
		n.queue_free()
	
	var button:Button
	for i in range(treament_limit):
		button = Button.new()
		button.text = self.treaments[i]+"  "+str(pv_prices[i])+"€"
		button.theme = button_theme
		button.mouse_default_cursor_shape = Control.CURSOR_POINTING_HAND
		# warning-ignore:return_value_discarded
		button.connect("pressed", self, "_on_pvButton_pressed", [ i ])
		$objectsButtons/pv.add_child(button)
		button = Button.new()
		button.text = self.treaments[i+5]+"  "+str(pe_prices[i])+"€"
		button.theme = button_theme
		button.mouse_default_cursor_shape = Control.CURSOR_POINTING_HAND
		# warning-ignore:return_value_discarded
		button.connect("pressed", self, "_on_peButton_pressed", [ i ])
		$objectsButtons/pe.add_child(button)
	for i in range(gifts_limit):
		button = Button.new()
		button.text = self.gifts[i]+"  "+str(gifts_prices[i])+"€"
		button.theme = button_theme
		button.mouse_default_cursor_shape = Control.CURSOR_POINTING_HAND
		# warning-ignore:return_value_discarded
		button.connect("pressed", self, "_on_giftsButton_pressed", [ i ])
		$objectsButtons/gifts.add_child(button)


func _on_return_pressed():
	self.visible = false


func _on_pvButton_pressed(button_number):
	sum = wanted_number*pv_prices[button_number]
	if sum > PlayerInfo.money:
		$Confirm.dialog_text = "Vous n'avez pas assez d'argent"
		player_have_enough_money = false
	else:
		$Confirm.dialog_text = "Voulez vous acheter "+str(wanted_number)+" "+treaments[button_number]+" pour "+str(sum)+"€ ?"
		player_have_enough_money = true
	purshased_num = button_number
	what_is_purchased = "pv"
	$Confirm.popup_centered()

func _on_peButton_pressed(button_number):
	sum = wanted_number*pe_prices[button_number]
	if sum > PlayerInfo.money:
		$Confirm.dialog_text = "Vous n'avez pas assez d'argent"
		player_have_enough_money = false
	else:
		$Confirm.dialog_text = "Voulez vous acheter "+str(wanted_number)+" "+treaments[button_number+5]+" pour "+str(sum)+"€ ?"
		player_have_enough_money = true
	purshased_num = button_number
	what_is_purchased = "pe"
	$Confirm.popup_centered()

func _on_giftsButton_pressed(button_number):
	sum = wanted_number*gifts_prices[button_number]
	if sum > PlayerInfo.money:
		$Confirm.dialog_text = "Vous n'avez pas assez d'argent"
		player_have_enough_money = false
	else:
		$Confirm.dialog_text = "Voulez vous acheter "+str(wanted_number)+" "+gifts[button_number]+" cadeaux pour "+str(sum)+"€ ?"
		player_have_enough_money = true
	what_is_purchased = "gifts"
	purshased_num = button_number
	$Confirm.popup_centered()

func _on_ButtonUp_pressed():
	wanted_number += 1
	$HBoxContainer/VBoxContainer/number.text = str(wanted_number)

func _on_ButtonDown_pressed():
	if wanted_number > 1:
		wanted_number -= 1
		$HBoxContainer/VBoxContainer/number.text = str(wanted_number)


func _on_Confirm_confirmed():
	if player_have_enough_money:
		match what_is_purchased:
			"pv":
				PlayerInfo.bag_treatment[treaments[purshased_num]] += wanted_number
			"pe":
				PlayerInfo.bag_treatment[treaments[purshased_num+5]] += wanted_number
			"gifts":
				PlayerInfo.bag_gifts[gifts[purshased_num]] += wanted_number
		PlayerInfo.money -= sum
		$HBoxContainer/money.text = "Vous avez "+str(PlayerInfo.money)+"€"
