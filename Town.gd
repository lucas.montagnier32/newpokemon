extends Node2D


# tableau des Elemon se trouvant dans cette scène par leur indice dans CurrentScene
# [id_elemon,levelmin] le level sera entre levelmin et levelmin+3
const fightermons = [[3,1],[6,1],[9,1]]


# Called when the node enters the scene tree for the first time.
func _ready():
	if PlayerInfo.current_mission>=3:
		$YSort/PNJM2.queue_free()
	if PlayerInfo.current_mission>=4:
		$YSort/TreeBlocus.queue_free()
	if PlayerInfo.current_mission>=5:
		$YSort/PNJM4.queue_free()


# MISSION 2
func _on_mission2_body_entered(_body):
	if(PlayerInfo.current_mission==2):
		print("start mission 2")
		Utils.get_player().stop_input = true
		$YSort/PNJM2.move([Vector2(0,-3)])


func _on_PNJM2_mission2_finished():
	$YSort/PNJM2.queue_free()


# MISSION 4
func _on_mission4_body_entered(_body):
	if PlayerInfo.current_mission==4:
		print("start mission 4")
		Utils.get_player().is_earthquake = true


func _on_PNJM4_mission4_finished():
	$YSort/PNJM4.queue_free()
