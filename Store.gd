extends Node2D

var gui

# Called when the node enters the scene tree for the first time.
func _ready():
	gui = Utils.get_GUI()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if Input.is_action_just_pressed("a"):
		if $Player.position == Vector2(16,-16) && $Player.facing_direction == 1:
			gui.display_store_interface(2,2)
