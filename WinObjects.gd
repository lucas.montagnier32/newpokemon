extends CanvasLayer


# Called when the node enters the scene tree for the first time.
func _ready():
	$Control.visible = false


func player_won_objects(objects:String):
	$Control/objects.text = objects
	$Control/HBoxContainer/TextureRect.rect_min_size = Vector2(20+4*objects.length(),24)
	$AnimationPlayer.play("fadeToDisappear")
