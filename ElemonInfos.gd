extends Control

var loaded_elemon:Fightermon.Fightermon_class

# Called when the node enters the scene tree for the first time.
func _ready():
	self.visible = false

#première page a être appelé
func first_load_elemonInfos(elemon_number):
	if(PlayerInfo.nb_elemons_f>=elemon_number):
		$TextureRect.modulate = Color($headerControl/header/infos.modulate)
		loaded_elemon = PlayerInfo.get_elemon(elemon_number)
		$headerControl/name.text = loaded_elemon.get_name()
		$elemonImage.texture = load(loaded_elemon.get_image())
		load_elemonInfos()
		self.visible = true


#première page
func load_elemonInfos():
	$infos.visible = true
	$capaControl.visible = false
	$TextureRect.modulate = Color($headerControl/header/infos.modulate)
	$infos/up.text = "Type: "+loaded_elemon.get_type1()+" "+loaded_elemon.get_type2()+"\nPuissance: "+loaded_elemon.get_string_rarity()
	$infos/values/name.text = "Niveau: \nNiveauSpe: "
	$infos/values/value.text = str(loaded_elemon.level)+"\n"+str(loaded_elemon.level_spe)
	$infos/values/name2.text = "Xp: \nXpSpe: "
	$infos/values/value2.text = str(loaded_elemon.xp)+"/"+str(loaded_elemon.calculate_next_level_xp())+"\n"+str(loaded_elemon.xp_spe)+"/"+str(loaded_elemon.calculate_next_level_xp_spe())
	$infos/down.text = "Type d'évolution: "+loaded_elemon.get_evolveType()


#deuxième page
func load_elemonStats():
	$infos.visible = true
	$capaControl.visible = false
	$TextureRect.modulate = Color($headerControl/header/stats.modulate)
	$infos/up.text = loaded_elemon.get_string_eval()
	$infos/values/name.text = "PV: \nAtt: \nDef: \nProbaCrit: "
	$infos/values/value.text = str(loaded_elemon.current_pv)+"/"+str(loaded_elemon.get_pvmax())+"("+str(loaded_elemon.pvmaxBonus)+")"+"\n"+str(round(loaded_elemon.get_att()))+"("+str(loaded_elemon.attBonus)+")"+"\n"+str(round(loaded_elemon.get_def()))+"("+str(loaded_elemon.defBonus)+")"+"\n"+str(loaded_elemon.probaCrit)+"%"
	$infos/values/name2.text = "PE: \nAttSpe: \nDefSpe: \nDegatsCrit: "
	$infos/values/value2.text = str(loaded_elemon.current_pe)+"/"+str(loaded_elemon.get_pemax())+"("+str(loaded_elemon.pemaxBonus)+")"+"\n"+str(round(loaded_elemon.get_attspe()))+"("+str(loaded_elemon.attspeBonus)+")"+"\n"+str(round(loaded_elemon.get_defspe()))+"("+str(loaded_elemon.defspeBonus)+")"+"\nx"+str(loaded_elemon.degatsCrit)
	$infos/down.text = "Vitesse: "+str(loaded_elemon.get_speed())+"("+str(loaded_elemon.speedBonus)+")\n"+PlayerInfo.researcher_buff_to_string()

#troisième page
func load_elemonCapas():
	$infos.visible = false
	$capaControl.visible = true
	$TextureRect.modulate = Color($headerControl/header/capas.modulate)
	
	var nbCapa = loaded_elemon.capacites.size()
	var capa = loaded_elemon.capacites[0]
	$capaControl/capa/capa1.text = "Nom: "+capa[0]+"   Type: "+capa[1]+"  ("+bool_to_isPhysicalString(capa[2])+")\n Puissance: "+str(capa[3])+"   coût Pe: "+str(capa[4])
	if(nbCapa>1):
		capa = loaded_elemon.capacites[1]
		$capaControl/capa/capa2.text = "Nom: "+capa[0]+"   Type: "+capa[1]+"  ("+bool_to_isPhysicalString(capa[2])+")\n Puissance: "+str(capa[3])+"   coût Pe: "+str(capa[4])
	else:
		$capaControl/capa/capa2.text = "vide\n"
	if(nbCapa>2):
		capa = loaded_elemon.capacites[2]
		$capaControl/capa/capa3.text = "Nom: "+capa[0]+"   Type: "+capa[1]+"  ("+bool_to_isPhysicalString(capa[2])+")\n Puissance: "+str(capa[3])+"   coût Pe: "+str(capa[4])
	else:
		$capaControl/capa/capa3.text = "vide\n"
	if(nbCapa>3):
		capa = loaded_elemon.capacites[3]
		$capaControl/capa/capa4.text = "Nom: "+capa[0]+"   Type: "+capa[1]+"  ("+bool_to_isPhysicalString(capa[2])+")\n Puissance: "+str(capa[3])+"   coût Pe: "+str(capa[4])
	else:
		$capaControl/capa/capa4.text = "vide\n"

func bool_to_isPhysicalString(b:bool):
	if(b):
		return "physique"
	else:
		return "spécial"

#---------------------------------------------------------------------------------------------------
# button action
func _on_return_pressed():
	self.visible = false

func _on_stats_pressed():
	load_elemonStats()

func _on_infos_pressed():
	load_elemonInfos()

func _on_capas_pressed():
	load_elemonCapas()
