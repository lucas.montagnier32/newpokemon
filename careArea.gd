extends Node2D

const SPEED = 20
const HEALING_WAIT_TIME = 1

var healing:bool

# Called when the node enters the scene tree for the first time.
func _ready():
	$Sprite.rotation_degrees = 0
	healing = false


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if(healing):
		$Sprite.rotation_degrees -= 4*SPEED*delta
	else:
		$Sprite.rotation_degrees += SPEED*delta

# fonction appelé quand un corps est en collision avec le CollisionShape2D
func _on_Area2D_body_entered(_body):
	$Timer.wait_time = HEALING_WAIT_TIME
	$Timer.start()
	healing = true
	$Sprite.modulate = Color("ffffff")


func _on_Area2D_body_exited(_body):
	healing = false
	$Sprite.modulate = Color("bfbfbf")


func _on_Timer_timeout():
	PlayerInfo.heal_all_by(2+PlayerInfo.healing_speed_gain,1+PlayerInfo.healing_speed_gain,1)
	Utils.get_GUI().update_GUI()
