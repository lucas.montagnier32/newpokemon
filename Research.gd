extends Control

const player_research_infos:Array = [
	"Augmentation du charisme + 1",
	"Limite du niveau des elemons + 2",
	"Xp de joueur + 1",
	"Vitesse de guérison des\nzones de soins +1/s",
	"infos 5","infos 6","infos 7",
	"infos 8","infos 9",
	"infos 10",
]
const elemons_research_infos:Array = [
	"Gain d'xp pour\ntous vos elemons + 5%",
	"Gain d'xp spécial\npour tous vos elemons + 5%",
	"PV max pour tous\nvos elemons + 3",
	"PE max pour tous\nvos elemons + 2",
	"infos 5","infos 6","infos 7",
	"infos 8","infos 9",
	"infos 10",
]
const types_research_infos:Array = [
	"Dégats des capacités du\nmême type que l’elemon + 5%",
	"Dégats des attaques\nsuper efficace + 5%",
	"Resistance aux attaques\nsuper efficace + 5%",
	"Resistance aux attaques du\nmême type que vos elemon + 5%",
	"infos 5","infos 6","infos 7",
	"infos 8","infos 9",
	"infos 10",
]

onready var down_research = $down/research

enum graph {PLAYER, ELEMONS, TYPES}
var current_graph:int
var current_node_number:int
var nodes_buttons:Array

# représente les prédecesseur de chaque node (1 ligne du tableau = 1 colonne du graphe)
var predecessors:Array = [
	[],
	[0], [0], [0],
	[1], [2], [3],
	[4,5], [5,6],
	[7,8],
]

# Called when the node enters the scene tree for the first time.
func _ready():
	self.visible = false
	self.current_graph = graph.PLAYER
	$down/infosLabel.text = ""
	down_research.visible = false
	nodes_buttons = $ScrollContainer/graphNodes/Buttons.get_children()
	# init des connexions des boutons du graphe
	for i in range(0, nodes_buttons.size()):
		nodes_buttons[i].connect("pressed", self, "_on_graphNode_pressed", [ i ])


func load_page():
	self.visible = true
	_on_player_pressed()


func darken_Left_button(number:int):
	var left_buttons = $Left/LeftButton.get_children()
	var left_labels = $Left/LeftText.get_children()
	for i in range(0, left_buttons.size()):
		if i==number-1:
			left_buttons[i].self_modulate = Color("888888")
			left_labels[i].self_modulate = Color("aaaaaa")
		else:
			left_buttons[i].self_modulate = Color.white
			left_labels[i].self_modulate = Color.white


# colorie chaques node en fonction de son status
func load_graph(bool_research_array:Array):
	for ind in range(0, bool_research_array.size()):
		if !bool_research_array[ind]:
			if PlayerInfo.player_research_points>0 && all_predecessors_are_true(bool_research_array, ind):
				nodes_buttons[ind].modulate = Color("888888")
			else:
				nodes_buttons[ind].modulate = Color("222222")
		else:
			nodes_buttons[ind].modulate = Color("ffffff")


# retourne vrai si les prédeceseur du node i sont tous a vrai dans bool_research_array
func all_predecessors_are_true(bool_research_array:Array, node_number:int) -> bool:
	for i in self.predecessors[node_number]:
		if !bool_research_array[i]:
			return false
	return true


func update_header():
	$down/infosLabel.text = ""
	$down/research.visible = false
	match self.current_graph:
		graph.PLAYER:
			$headerLabel.text = "Graphe de recherche des améliorations sur le joueur ("+str(PlayerInfo.player_research_points)+" points)"
		graph.ELEMONS:
			$headerLabel.text = "Graphe de recherche des améliorations sur les elemons ("+str(PlayerInfo.elemons_research_points)+" points)"
		graph.TYPES:
			$headerLabel.text = "Graphe de recherche des améliorations sur les types ("+str(PlayerInfo.types_research_points)+" points)"

#---------------------------------------------------------------------------------------------------
# button action
func _on_return_pressed():
	Utils.get_GUI().update_GUI()
	self.visible = false

func _on_player_pressed():
	darken_Left_button(1)
	load_graph(PlayerInfo.player_bool_research_array)
	self.current_graph = graph.PLAYER
	update_header()

func _on_elemon_pressed():
	darken_Left_button(2)
	load_graph(PlayerInfo.elemons_bool_research_array)
	self.current_graph = graph.ELEMONS
	update_header()

func _on_types_pressed():
	darken_Left_button(3)
	load_graph(PlayerInfo.types_bool_research_array)
	self.current_graph = graph.TYPES
	update_header()

# quand appuie sur le bouton d'un node du graphe
func _on_graphNode_pressed(node_number:int):
	current_node_number = node_number
	match self.current_graph:
		graph.PLAYER:
			$down/infosLabel.text = self.player_research_infos[node_number]
			if PlayerInfo.player_bool_research_array[node_number] || nodes_buttons[node_number].modulate==Color("222222"): # pas de bouton recherche si déja recherché ou pas dispo
				down_research.visible = false
			else:
				down_research.visible = true
		graph.ELEMONS:
			$down/infosLabel.text = self.elemons_research_infos[node_number]
			if PlayerInfo.elemons_bool_research_array[node_number] || nodes_buttons[node_number].modulate==Color("222222"): # pas de bouton recherche si déja recherché ou pas dispo
				down_research.visible = false
			else:
				down_research.visible = true
		graph.TYPES:
			$down/infosLabel.text = self.types_research_infos[node_number]
			if PlayerInfo.types_bool_research_array[node_number] || nodes_buttons[node_number].modulate==Color("222222"): # pas de bouton recherche si déja recherché ou pas dispo
				down_research.visible = false
			else:
				down_research.visible = true


# action du bouton research en bas
func _on_researchButton_pressed():
	match self.current_graph:
		graph.PLAYER:
			if PlayerInfo.player_research_points>0:
				PlayerInfo.player_research_points -= 1
				PlayerInfo.player_bool_research_array[current_node_number] = true
				load_graph(PlayerInfo.player_bool_research_array)
				down_research.visible = false
				update_header()
				PlayerInfo.update_player_research_bonus()
		graph.ELEMONS:
			if PlayerInfo.elemons_research_points>0:
				PlayerInfo.elemons_research_points -= 1
				PlayerInfo.elemons_bool_research_array[current_node_number] = true
				load_graph(PlayerInfo.elemons_bool_research_array)
				down_research.visible = false
				update_header()
				apply_elemons_one_shot_research(current_node_number)
				PlayerInfo.update_elemons_research_bonus()
		graph.TYPES:
			if PlayerInfo.types_research_points>0:
				PlayerInfo.types_research_points -= 1
				PlayerInfo.types_bool_research_array[current_node_number] = true
				load_graph(PlayerInfo.types_bool_research_array)
				down_research.visible = false
				update_header()
				PlayerInfo.update_types_research_bonus() 


func apply_elemons_one_shot_research(node_number):
	match node_number:
		2: for e in PlayerInfo.get_all_elemons():
				if e!=null:
					e.pvmax += 3
					e.current_pv += 3
		3: for e in PlayerInfo.get_all_elemons():
				if e!=null:
					e.pemax += 2
					e.current_pe += 2
		_: print("node number "+str(node_number)+" not assigned")


