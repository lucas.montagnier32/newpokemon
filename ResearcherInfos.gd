extends Control

var loaded_researcher:Researcher.Researcher_class

# Called when the node enters the scene tree for the first time.
func _ready():
	self.visible = false

# researcher_number = 1, 2 ou 3
func load_page(researcher_number):
	if PlayerInfo.nb_researcher >= researcher_number:
		loaded_researcher = PlayerInfo.get_all_researcher()[researcher_number-1]
		$VBoxContainer/infos1.text = loaded_researcher.get_name()+"\nEnergie: "+str(loaded_researcher.current_energy)+"/"+str(loaded_researcher.get_energyMax())
		$VBoxContainer/infos2.text = "Force de recherche elemons (ERP): "+str(loaded_researcher.get_elemons_research_power())+"\nForce de recherche types (TRP): "+str(loaded_researcher.get_types_research_power())
		$VBoxContainer/infos3.text = "Pourcentage de recherche elemons: "+str(loaded_researcher.elemons_research_percent)+"%\nPourcentage de recherche types: "+str(loaded_researcher.types_research_percent)+"%\nBuff: "+loaded_researcher.get_buff()+" +15%"
		self.visible = true

#---------------------------------------------------------------------------------------------------
# button action
func _on_return_pressed():
	self.visible = false
