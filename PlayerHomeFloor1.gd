extends Node2D

# Called when the node enters the scene tree for the first time.
func _ready():
	if(PlayerInfo.current_mission>=2):
		delete_all_pnj()


func _on_PNJ1_mission1_finished():
	delete_all_pnj()


func delete_all_pnj():
	$YSort/PNJ1.queue_free()
	$YSort/PNJ2.queue_free()


# MISSION 1
var pnj1
var pnj2
func _on_mission1_body_entered(_body):
	if(PlayerInfo.current_mission==1):
		print("start mission 1")
		Utils.get_player().stop_input = true
		pnj1 = Utils.get_pnj_from_id(1)
		pnj2 = Utils.get_pnj_from_id(2)
		pnj1.move([Vector2(0, -2), Vector2(4, 0)])
		pnj2.move([Vector2(2, 0), Vector2(0, -1)])
