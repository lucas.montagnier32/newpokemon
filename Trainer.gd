extends KinematicBody2D

# taille constante d'une tuile
const TILE_SIZE = 16

# direction initial du pnj
export var initial_direction:Vector2
export var texts:Array # faire un text before fight et un after fight ?
# portée de sa vision en nombre de tuile
export var range_vision:int

# var spécifique a chaques trainer
export var id_trainer:int # pour reconnaitre le trainer
export var is_defeated:bool
export var type:int # 0 pour fight, 1 pour pacific
export var id_and_lvl_elemons:PoolVector2Array # x= id_elemon, y= level
export var elemons_power:int = Fightermon.rarity_enum.NORMAL

onready var pnj_sprite = $Sprite
onready var speek_label = $SpeekLabel
onready var ray_cast = $RayCast2D

var player
var player_sprite
var power

# Called when the node enters the scene tree for the first time.
func _ready():
	# init de la driection
	match initial_direction:
		Vector2(-1,0):
			pnj_sprite.frame = 7
			pnj_sprite.flip_h = false
		Vector2(1,0):
			pnj_sprite.frame = 7
			pnj_sprite.flip_h = true
		Vector2(0,-1):
			pnj_sprite.frame = 4
		Vector2(0,1):
			pnj_sprite.frame = 1
	# init de la direction du rayCast
	ray_cast.cast_to = initial_direction*range_vision*TILE_SIZE
	ray_cast.force_raycast_update()
	
	player = Utils.get_player()
	player_sprite = player.find_node("Sprite")
	
	# init de is_defeated si le tableau stocké contient l'id de ce dresseur
	self.is_defeated = PlayerInfo.defeatedTrainerId.has(self.id_trainer)
	
	self.power = 0
	for v in id_and_lvl_elemons:
		self.power+=v.y
	
	# connection des signaux
	player.connect("player_want_to_talk_signal", self, "talk_to_trainer")


func talk_to_trainer():
	if is_player_near():
		player.stop_input_for_pnj = true
		Utils.initiate_dialog(self.texts)


func is_player_near() -> bool:
	return Vector2(abs(player.position.x-self.position.x),abs(player.position.y-self.position.y))<=Vector2(range_vision*TILE_SIZE, range_vision*TILE_SIZE)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if(!player.stop_input_for_pnj and !self.is_defeated and PlayerInfo.nb_elemons_f>0):
		if player.begin_fight and is_player_near():
			Utils.get_scene_manager().transition_to_trainer_battle(player_sprite.frame, player_sprite.flip_h, id_and_lvl_elemons, id_trainer, self.elemons_power)
		else:
			ray_cast.force_raycast_update()
			if(ray_cast.is_colliding()):
				if(PlayerInfo.defeatedTrainerId.has(self.id_trainer)):
					self.is_defeated = true
				else:
					print("call talk_to_trainer")
					talk_to_trainer()
	# affichage de la puissance si joueur proche
	if !self.is_defeated && Vector2(abs(player.position.x-self.position.x),abs(player.position.y-self.position.y))<=Vector2(2*TILE_SIZE, 2*TILE_SIZE):
		$PowerLabel.text = "Puissance: "+str(power)
		$PowerLabel.visible = true
	else:
		$PowerLabel.visible = false
