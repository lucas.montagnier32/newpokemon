extends Node2D

var the_texts:Array

const ind_typing_text = 4 # indice où le joueur doit saisir son nom
const ind_name = 5 # indice où il faut rajouter le nom du joueur

onready var label_text = $Control/text
onready var next_texture = $Control/next

var percent_time_between_text = 0 # temps entre chaque texte en fonction de la taille du texte
var num_text:int = 0
var nb_texts:int
var transition:bool = false
var text_size:float = 0.0
var is_language_choosed:bool = false
var is_typing:bool = false

var display_speed:float

# Called when the node enters the scene tree for the first time.
func _ready():
	$Control.visible = false
	$Languages.visible = true


func init_intro_texts():
	# initialisation des textes de l'intro
	var i = 1
	while tr("intro"+str(i)) != ("intro"+str(i)):
		the_texts.push_back(tr("intro"+str(i)))
		i += 1
	label_text.text = the_texts[0]
	nb_texts = the_texts.size()
	# calcul de la vitesse d'affichage
	text_size = the_texts[num_text].length()
	display_speed = (30/text_size)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if(!transition && is_language_choosed):
		percent_time_between_text += (display_speed*delta)
		label_text.percent_visible = percent_time_between_text
		if(percent_time_between_text>=1):
			next_texture.visible = true
			if(Input.is_action_just_pressed("ok") or Input.is_action_just_pressed("ui_accept")):# passage au prochain texte
				if(is_typing):
					typing_management()
				else:
					text_progress_management()


func typing_management():
	if($Control/LineEdit.text.length()>1 && $Control/LineEdit.text.length()<=12): # pas d'avancement tant que la taille est inférieur a 2
		if(!$Control/ConfirmationDialog.visible):
			$Control/ConfirmationDialog.popup_centered()
	elif $Control/LineEdit.text.length()<=1:
		$Control/LineEdit.placeholder_text = "nom trop court"
		$Control/LineEdit.clear()
	else:
		$Control/LineEdit.placeholder_text = "nom trop long"
		$Control/LineEdit.clear()


func text_progress_management():
	next_texture.visible = false
	num_text += 1
	if(num_text>=nb_texts): # si fin des textes
		transition = true
		Utils.get_scene_manager().transitionToScene("res://PlayerHomeFloor2.tscn",Vector2(128,80),Vector2(0,1))
	else:
		label_text.text = the_texts[num_text] # maj du texte
		percent_time_between_text = 0
		label_text.percent_visible = 0
		# calcul de la vitesse d'affichage du prochain texte
		text_size = the_texts[num_text].length()
		display_speed = (30/text_size)
		if(num_text==ind_typing_text): # montre le lineEdit
			$Control/LineEdit.visible = true
			is_typing = true
		else:
			$Control/LineEdit.visible = false


func _on_ConfirmationDialog_confirmed():
	$Control/LineEdit.visible = false
	PlayerInfo.player_name = $Control/LineEdit.text
	is_typing = false
	next_texture.visible = true
	the_texts[ind_name] += " "+$Control/LineEdit.text
	$Control/text.text = "C'est noté !"


func _on_lang_pressed(button_num):
	var lang_buttons:Array = $Languages.get_children()
	lang_buttons[button_num-1].pressed = true
	match button_num:
		1:
			TranslationServer.set_locale("en")
		2:
			TranslationServer.set_locale("es")
		3:
			TranslationServer.set_locale("fr")
	$Languages.visible = false
	$Control.visible = true
	is_language_choosed = true
	init_intro_texts()
