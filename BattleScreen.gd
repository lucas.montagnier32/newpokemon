extends Node2D

enum Who { ALLIE, ENNEMI }

# buff entier pour chaque elemons
var buff_pv:Array = [0,0,0,0,0]
var buff_pe:Array = [0,0,0,0,0]

# current pokemon affiché
var ennemi:Fightermon.Fightermon_class
# tout les pokemon de l'équipe
var ennemies:Array
var i_current_ennemi:int=0
var nb_ennemies_alive:int
# current pokemon affiché
var allie:Fightermon.Fightermon_class
# tout les pokemon de l'équipe
var allies:Array
var i_current_ally:int=0
var nb_allies_alive:int

var heal_base:float = 1

# save le spawn location and direction
var player_location = Vector2(0,0)
var player_direction = Vector2(0,0)

var the_faster

# pour calculé l'xp gagné en fonction du nombre de tour
var nbTour:int

var battle_is_finished:bool = false
# 2=pas d'affichage ; 1=affichage stats+1 ; 0=affichage stats finales
var displaying_level_up:int = 2
var is_player_win:bool
var is_trainer_battle:bool
var is_elemon_join_team:bool = false
var is_any_place_in_the_team:bool

onready var alliePVProgressBar:ProgressBar = $Allie/ProgressBar
onready var ennemiPVProgressBar:ProgressBar = $Ennemi/ProgressBar

onready var menu_principal = $Control/Menu
onready var menu_capacites = $Control/CapacitesNode
onready var xp_label = $Allie/VBoxContainer/HBoxContainer2/xpLabel
onready var xp_spe_label = $Allie/VBoxContainer/HBoxContainer2/xpSpeLabel
onready var team_manager = $TeamManager
onready var description = $Description
onready var anim = $AnimationPlayer
onready var xpBar = $Allie/VBoxContainer/HBoxContainer3/xpBar
onready var xpSpeBar = $Allie/VBoxContainer/HBoxContainer3/xpSpeBar

# pour les attaques
var capa_allie:Array
var capa_ennemi:Array
var allie_have_already_attacked:bool = false
var ennemi_have_already_attacked:bool = false

# pour la monté de l'xp
var xp_increasing:bool
var xp_gained:int
var xp_spe_gained:int

# Called when the node enters the scene tree for the first time.
func _ready():
	menu_principal.visible = true
	menu_capacites.visible = false
	$LevelUp.visible = false
	$LevelSpeUp.visible = false
	team_manager.visible = false
	
	allies.insert(0,PlayerInfo.elemon11)
	allies.insert(1,PlayerInfo.elemon12)
	allies.insert(2,PlayerInfo.elemon13)
	allies.insert(3,PlayerInfo.elemon14)
	allies.insert(4,PlayerInfo.elemon15)
	
	
	nb_allies_alive = 0
	var i = 0
	for a in allies:
		if a!=null:
			nb_allies_alive +=1
			# calcul des buff de pv et pe qui seront retiré à la fin du combat
			buff_pv[i] = int(round(a.pvmax*(PlayerInfo.get_researcher_buff("pv")-1.0)))
			buff_pe[i] = int(round(a.pemax*(PlayerInfo.get_researcher_buff("pe")-1.0)))
			a.current_pv += buff_pv[i]
			a.current_pe += buff_pe[i]
			a.pvmax += buff_pv[i]
			a.pemax += buff_pe[i]
			i += 1
	
	Utils.get_scene_manager().connect("elemons_loaded_signal",self,"display_elemons")
	
	# warning-ignore:return_value_discarded
	$PlayerBag.connect("gift_used", self, "_on_gift_used")
	# warning-ignore:return_value_discarded
	$PlayerBag.connect("treatment_used", self, "_on_treatment_used")


#---------------------------------------------------------------------------------------------------
#
func _input(event):
	if(battle_is_finished && !xp_increasing && displaying_level_up==2 && !$LevelSpeUp.visible && event.is_action_pressed("ok")):
		for i in range(0, nb_allies_alive): # remise aux stats normales sans buff pv pe
			allies[i].current_pv -= buff_pv[i]
			allies[i].current_pe -= buff_pe[i]
			allies[i].pvmax -= buff_pv[i]
			allies[i].pemax -= buff_pe[i]
		if(is_trainer_battle):
			Utils.get_scene_manager().transition_exit_trainer_battle(PlayerInfo.current_scene, is_player_win)
		else:
			Utils.get_scene_manager().transition_exit_battle(PlayerInfo.current_scene, is_player_win, false)
	
	if(displaying_level_up==1 && event.is_action_pressed("ok")):
		display_new_stats_level_up()
	elif(displaying_level_up==0 && event.is_action_pressed("ok")):
		$LevelUp.visible = false
		$Control.visible = true
		displaying_level_up=2


#---------------------------------------------------------------------------------------------------
#
func _on_FleeButton_pressed():
	if(!$LevelSpeUp.visible):
		if(!is_trainer_battle):
			for i in range(0, nb_allies_alive): # remise aux stats normales sans buff pv pe
				allies[i].current_pv -= buff_pv[i]
				allies[i].current_pe -= buff_pe[i]
				allies[i].pvmax -= buff_pv[i]
				allies[i].pemax -= buff_pe[i]
			$AnimationPlayer.play("music_fade")
			Utils.get_scene_manager().transition_exit_battle(PlayerInfo.current_scene,false, true)
		else:
			$Description/Label.text = "Vous ne pouvez pas fuir"

#---------------------------------------------------------------------------------------------------
# affiche les elemons une fois que le tableau ennemie est chargé
func display_elemons():
	# trouve un allie
	i_current_ally = find_a_living_ally()
	maj_xp_label_allie()
	update_pv_allie()
	
	# chargement des images
	var elemon_sprite = $Allie/Sprite
	elemon_sprite.texture = load(allie.get_image())
	elemon_sprite.region_enabled = true
	elemon_sprite.region_rect.position = Vector2(0,100)
	elemon_sprite.region_rect.size = Vector2(60,60)
	
	i_current_ennemi = 0
	ennemi = ennemies[i_current_ennemi]
	nb_ennemies_alive = ennemies.size()
	elemon_sprite = $Ennemi/Sprite
	elemon_sprite.texture = load(ennemi.get_image())
	elemon_sprite.region_enabled = true
	elemon_sprite.region_rect.position = Vector2(0,35)
	elemon_sprite.region_rect.size = Vector2(60,60)
	
	update_both_infos_label()
	
	# set de thefaster
	if(allie.get_speed()*PlayerInfo.get_researcher_buff("spd")>=ennemi.get_speed()):
		the_faster = Who.ALLIE
	else:
		the_faster = Who.ENNEMI


# retourne un entier correspondant à l'indice d'un allié en bonne santé
func find_a_living_ally():
	# trouve un allie
	var i:int = 0
	while(allies[i]==null || allies[i].current_pv<=0):
		i += 1
	allie = allies[i]
	return i

#---------------------------------------------------------------------------------------------------
func update_elemon_allie():
	# image
	var elemon_sprite = $Allie/Sprite
	elemon_sprite.texture = load(allie.get_image())
	elemon_sprite.modulate = "ffffffff" # remise en normal du modulate mis en tranparent par l'animation
	elemon_sprite.region_enabled = true
	elemon_sprite.region_rect.position = Vector2(0,100)
	elemon_sprite.region_rect.size = Vector2(60,60)
	# stats affiché
	var container_child_array = $Allie/VBoxContainer/HBoxContainer.get_children()
	container_child_array[0].text = allie.get_name()+" Lvl "+str(allie.level)
	container_child_array[1].text = "  PV: "+str(allie.current_pv)+"/"+str(allie.get_pvmax())
	container_child_array[2].text = "  PE: "+str(allie.current_pe)+"/"+str(allie.get_pemax())
	container_child_array[3].text = allie.get_type1()+","+allie.get_type2()
	$Allie/VBoxContainer/HBoxContainer/rarity.text = allie.get_string_rarity()
	# health bar
	update_pv_allie()
	# xp et xpspe
	maj_xp_label_allie()
	# capacites
	update_capacites_allie()


#---------------------------------------------------------------------------------------------------
func update_elemon_ennemi():
	# image
	var elemon_sprite = $Ennemi/Sprite
	elemon_sprite.texture = load(ennemi.get_image())
	elemon_sprite.modulate = "ffffffff" # remise en normal du modulate mis en tranparent par l'animation
	elemon_sprite.region_enabled = true
	elemon_sprite.region_rect.position = Vector2(0,35)
	elemon_sprite.region_rect.size = Vector2(60,60)
	# stats affiché
	var container_child_array = $Ennemi/HBoxContainer.get_children()
	container_child_array[0].text = ennemi.get_name()+" Lvl "+str(ennemi.level)
	container_child_array[1].text = "  PV: "+str(ennemi.current_pv)+"/"+str(ennemi.get_pvmax())
	container_child_array[2].text = "  PE: "+str(ennemi.current_pe)+"/"+str(ennemi.get_pemax())
	container_child_array[3].text = ennemi.get_type1()+","+ennemi.get_type2()
	$Ennemi/HBoxContainer/rarityLabel.text = ennemi.get_string_rarity()
	# health bar
	update_pv_ennemi()
	# capacites
	# TODO : update_capacites_ennemi()

#---------------------------------------------------------------------------------------------------
#
func _on_FightButton_pressed():
	if(!$LevelSpeUp.visible):
		# on enleve l'autre menu et ajoute les capacités
		menu_principal.visible = false
		menu_capacites.visible = true
		update_capacites_allie()


func update_capacites_allie():
	var capacites_allie = allie.capacites
	var size = capacites_allie.size()
	var capa = $Control/CapacitesNode/textCapa/VBoxContainer/capa1
	if(size>=1):
		capa.text = capacites_allie[0][0]+" \n("+str(capacites_allie[0][4])+" pe)  "+capacites_allie[0][1]+"\n puissance: "+str(capacites_allie[0][3])
	else:
		capa.text = ""
	capa = $Control/CapacitesNode/textCapa/VBoxContainer2/capa2
	if(size>=2):
		capa.text = capacites_allie[1][0]+" \n("+str(capacites_allie[1][4])+" pe)  "+capacites_allie[1][1]+"\n puissance: "+str(capacites_allie[1][3])
	else:
		capa.text = ""
	capa = $Control/CapacitesNode/textCapa/VBoxContainer/capa3
	if(size>=3):
		capa.text = capacites_allie[2][0]+" \n("+str(capacites_allie[2][4])+" pe) "+capacites_allie[2][1]+"\n puissance: "+str(capacites_allie[2][3])
	else:
		capa.text = ""
	capa = $Control/CapacitesNode/textCapa/VBoxContainer2/capa4
	if(size>=4):
		capa.text = capacites_allie[3][0]+" \n("+str(capacites_allie[3][4])+" pe)  "+capacites_allie[3][1]+"\n puissance: "+str(capacites_allie[3][3])
	else:
		capa.text = ""

#---------------------------------------------------------------------------------------------------
# passage du menu des capacités au menu général
func _on_returnButton_pressed():
	menu_principal.visible = true
	menu_capacites.visible = false


#---------------------------------------------------------------------------------------------------
# actions de chaques capacités alliés
# chaque bouton est lié a cette fonction avec un paramètre entier désignant chaque bouton
func _on_capa_pressed(extra_arg_0):
	if(extra_arg_0==9):# si il appuie sur le bouton +PE+
		action_of_PE_button()
	elif(allie.capacites.size()>=extra_arg_0):# sinon une capacité
		action_of_capa_buttons(extra_arg_0)


func action_of_PE_button():
	if(allie.current_pe<allie.get_pemax()):# si pas deja au max
		allie.current_pe += (allie.get_pemax()/2)
		if(allie.current_pe>allie.get_pemax()):# on dépasse pas le max
			allie.current_pe = allie.get_pemax()
		description.visible = true
		update_pe_allie()
		$Description/Label.text = allie.get_name()+" se repose\net regagne "+str(allie.get_pemax()/2)+" PE"
		allie_have_already_attacked = true
		menu_capacites.visible = false
		process_attack_of_ennemi()
		nbTour += 1
	else:
		$Description/Label.text = "Vous avez déja le max de PE"


func action_of_capa_buttons(num_capa:int):
	if(allie.current_pe>=allie.capacites[num_capa-1][4]):# en vérifiant qu'il a assez de pe
		description.visible = true
		menu_capacites.visible = false
		
		# choix des capacités pour les attaques (capa_allie est le tableau contenant les infos de la capacite
		capa_allie = allie.capacites[num_capa-1]
		
		if(the_faster==Who.ALLIE):
			process_attack_of_allie()
		else:
			process_attack_of_ennemi()
		nbTour += 1
	else:
		$Description/Label.text = allie.get_name()+" n'a pas assez de PE"

#---------------------------------------------------------------------------------------------------
func process_attack_of_allie():
	$Description/Label.text = allie.get_name()+" utilise "+capa_allie[0]+"\n"+efficiencie_description_text(Fightermon.stringCapa_to_int(capa_allie[1]), Fightermon.stringCapa_to_int(ennemi.get_type1()))
	allie.current_pe -= capa_allie[4]
	update_pe_allie()
	# animation de l'attaque
	who_is_attacked = Who.ENNEMI
	percent_pv_decrease=0.0
	old_current_pv = ennemi.current_pv
	if(capa_allie[2]):# si attaque physique
		nb_pv_decrease = allie_attack_value(allie.get_att(),capa_allie[3],ennemi.get_def(), Fightermon.stringCapa_to_int(capa_allie[1]), Fightermon.stringCapa_to_int(ennemi.get_type1()), allie.probaCrit, allie.degatsCrit)
	else:
		nb_pv_decrease = allie_attack_value(allie.get_attspe(),capa_allie[3],ennemi.get_defspe(), Fightermon.stringCapa_to_int(capa_allie[1]), Fightermon.stringCapa_to_int(ennemi.get_type1()), allie.probaCrit, allie.degatsCrit)
	is_attack_thrown = true # lancement de la descente des pv
	allie_have_already_attacked = true
	if nb_pv_decrease>0:
		$CPUParticles2D.emitting = true


func process_attack_of_ennemi():
	#init des capacités de l'ennemi
	capa_ennemi = ennemi.capacites[randi()%ennemi.capacites.size()]
	if(ennemi.current_pe>=capa_ennemi[4]):# si il a assez de pe
		$Description/Label.text = ennemi.get_name()+" utilise "+capa_ennemi[0]+"\n"+efficiencie_description_text(Fightermon.stringCapa_to_int(capa_ennemi[1]),Fightermon.stringCapa_to_int(allie.get_type1()))
		ennemi.current_pe -= capa_ennemi[4]
		update_pe_ennemi()
		# animation de l'attaque
		who_is_attacked = Who.ALLIE
		percent_pv_decrease=0.0
		old_current_pv = allie.current_pv
		if(capa_ennemi[2]):# si attaque physique
			nb_pv_decrease = ennemi_attack_value(ennemi.get_att(),capa_ennemi[3],int(allie.get_def()*PlayerInfo.get_researcher_buff("def")),Fightermon.stringCapa_to_int(capa_ennemi[1]),Fightermon.stringCapa_to_int(allie.get_type1()), ennemi.probaCrit, ennemi.degatsCrit)
		else:
			nb_pv_decrease = ennemi_attack_value(ennemi.get_attspe(),capa_ennemi[3],int(allie.get_defspe()*PlayerInfo.get_researcher_buff("def")),Fightermon.stringCapa_to_int(capa_ennemi[1]),Fightermon.stringCapa_to_int(allie.get_type1()), ennemi.probaCrit, ennemi.degatsCrit)
		if nb_pv_decrease>0:
			$CPUParticles2D2.emitting = true
	else:# sinon pe++
		ennemi.current_pe += (ennemi.get_pemax()/2)
		if(ennemi.current_pe>ennemi.get_pemax()):# on dépasse pas le max
			ennemi.current_pe = ennemi.get_pemax()
		description.visible = true
		update_pe_ennemi()
		$Description/Label.text = ennemi.get_name()+" se repose\net regagne "+str(ennemi.get_pemax()/2)+" PE"
		who_is_attacked = Who.ALLIE
		percent_pv_decrease=0.0
		old_current_pv = allie.current_pv
		nb_pv_decrease = 0
	is_attack_thrown = true # lancement de la descente des pv
	ennemi_have_already_attacked = true

#---------------------------------------------------------------------------------------------------
func is_ennemi_ko():
	return ennemi.current_pv<=0

func is_allie_ko():
	return allie.current_pv<=0


func ennemi_attack_value(att:int, percent_capa:int, def:int, type_capa:int, type_def:int, proba_crit_att:int, mult_crit_att:float) -> int:
	var technique_attack_value = (percent_capa*Fightermon.efficiencies[type_capa][type_def]/100.0)*ennemi_damage_research_bonus(type_capa, type_def)
	var offensive_attack_value = att*technique_attack_value*crit_attack_value(proba_crit_att, mult_crit_att)
	var final_value = int(ceil( (offensive_attack_value/def)*10 ))
	return final_value

func ennemi_damage_research_bonus(type_capa:int, type_def:int) -> float:
	var tech_damage_mult = 1.0
	if type_capa == type_def:
		tech_damage_mult -= PlayerInfo.ennemi_damage_capa_elemon_same_types
	if Fightermon.efficiencies[type_capa][type_def]==1.5:
		tech_damage_mult -= PlayerInfo.ennemi_damage_super_efficience
	return tech_damage_mult

func allie_attack_value(att:int, percent_capa:int, def:int, type_capa:int, type_def:int, proba_crit_att:int, mult_crit_att:float) -> int:
	var technique_attack_value = (percent_capa*Fightermon.efficiencies[type_capa][type_def]/100.0)*damage_research_bonus(Fightermon.stringCapa_to_int(allie.get_type1()), type_capa, type_def)
	var offensive_attack_value = att*technique_attack_value*PlayerInfo.get_researcher_buff("att")*crit_attack_value(int(proba_crit_att*PlayerInfo.get_researcher_buff("crit")), mult_crit_att)
	var final_value = int(ceil( (offensive_attack_value/def)*10 ))
	return final_value

# recherche
func damage_research_bonus(type_att:int, type_capa:int, type_def:int) -> float:
	var tech_damage_mult = 1.0
	if type_att==type_capa:
		tech_damage_mult += PlayerInfo.damage_capa_elemon_same_types
	if Fightermon.efficiencies[type_capa][type_def]==1.5:
		tech_damage_mult += PlayerInfo.damage_super_efficience
	return tech_damage_mult

# retourne le multiplicateur de degats critique si cout critique
func crit_attack_value(proba:int, mult:float):
	var random_gen = RandomNumberGenerator.new()
	random_gen.randomize()
	var random_number = random_gen.randi_range(0, 100)
	if(random_number>=proba):
		return 1
	else:
		$Description/Label.text += "Cout critique "
		return mult


# return the text to print on the description label
func efficiencie_description_text(type_att:int, type_def:int):
	var efficiencie = Fightermon.efficiencies[type_att][type_def]
	match efficiencie:
		0.5: return "Ce n'est pas très efficace. "
		1: return ""
		1.5: return "C'est super efficace ! "

#---------------------------------------------------------------------------------------------------
var percent_pv_decrease=0.0
var is_attack_thrown:bool = false
var spd:float = 0.5
var nb_pv_decrease:int = 10
var old_current_pv:int = 12
var who_is_attacked:int = Who.ENNEMI

var acc_xp:int = 0
var percent_xp:float = 0.0

var base_xp:int
var base_xp_spe:int

# fonction appelé a chaque image
func _process(delta):
	if(is_attack_thrown):
		if(percent_pv_decrease>=0.95 || is_a_ko()):# si il a fini de perdre des pv
			decreasing_end()
		else:
			continue_decreasing(delta)
	
	if(xp_increasing):
		if(percent_xp>=1):# fin
			xp_increasing = false
			percent_xp = 0
		else:
			percent_xp += (spd*delta)
			if(allie.set_xp(int(base_xp+(percent_xp*xp_gained)))):
				base_xp = 0
				display_level_up()
			if(allie.set_xp_spe(int(base_xp_spe+(percent_xp*xp_spe_gained)))):
				base_xp_spe = 0 
				display_level_spe_up()
			maj_xp_label_allie()


func decreasing_end():
	is_attack_thrown = false
	percent_pv_decrease=0
	if(who_is_attacked==Who.ALLIE): # qui perd les pv
		allie_is_attacked()
	elif(who_is_attacked==Who.ENNEMI):
		ennemi_is_attacked()


func continue_decreasing(delta):
	percent_pv_decrease = percent_pv_decrease+(spd*delta)
	if(who_is_attacked==Who.ALLIE):# qui perd les pv
		allie.current_pv = old_current_pv-(percent_pv_decrease*nb_pv_decrease)
		update_pv_allie()
	elif(who_is_attacked==Who.ENNEMI):
		ennemi.current_pv = old_current_pv-(percent_pv_decrease*nb_pv_decrease)
		update_pv_ennemi()


# refactoring (utilisé dans _process)
func allie_is_attacked():
	allie.current_pv = old_current_pv-nb_pv_decrease
	if(!is_allie_ko()):
		if(allie_have_already_attacked):
			display_menu_player() # remise a la normal
		else:
			process_attack_of_allie()
	elif(nb_allies_alive-1>0):# -1 car le current est mort
		allie.current_pv=0
		nb_allies_alive -= 1
		anim.play("fade_of_allie_ko")
	else:
		allie.current_pv=0
		battle_finished(Who.ENNEMI)


# refactoring (utilisé dans _process)
func ennemi_is_attacked():
	ennemi.current_pv = old_current_pv-nb_pv_decrease
	if(!is_ennemi_ko()):
		if(ennemi_have_already_attacked): # remise a la normal
			display_menu_player()
		else:
			process_attack_of_ennemi()
	elif(nb_ennemies_alive-1>0):# -1 car le current est mort
		nb_ennemies_alive -= 1
		anim.play("fade_of_ennemi_ko")
		gain_xp()
	else:
		battle_finished(Who.ALLIE)


# test si un des deux pokemon est ko
func is_a_ko():
	return (ennemi.current_pv<=0 || allie.current_pv<=0)


# appelé a la fin de l'animation fade_of_allie_ko
func change_allie():
	if(!battle_is_finished):# comme ça la fonction ne fait rien si l'animation été lancé a partir de battle_finished()
		allie = allies[i_current_ally+1]
		update_elemon_allie() # update affichage pour nouvel allie
		display_menu_player()


# appelé a la fin de l'animation fade_of_ennemi_ko
func change_ennemi():
	if(!battle_is_finished):# comme ça la fonction ne fait rien si l'animation été lancé a partir de battle_finished()
		ennemi = ennemies[i_current_ennemi+1]
		update_elemon_ennemi() # update affichage pour nouvel ennemi
		display_menu_player()
		nbTour=0

#---------------------------------------------------------------------------------------------------
func update_pv_allie():
	$Allie/VBoxContainer/HBoxContainer/pv.text = "  PV: "+str(allie.current_pv)+"/"+str(allie.get_pvmax())
	var percent_health:float = float(allie.current_pv)/float(allie.get_pvmax())
	if(allie.current_pv>(allie.get_pvmax()/2)): # plus de 50% de pv
		alliePVProgressBar.modulate = Color(0,1,0)
	elif(allie.current_pv>(allie.get_pvmax()/10)):
		alliePVProgressBar.modulate = Color(1,1,0)
	else:
		alliePVProgressBar.modulate = Color(1,0,0)
	alliePVProgressBar.value = percent_health*100


func update_pv_ennemi():
	$Ennemi/HBoxContainer/pv.text = "  PV: "+str(ennemi.current_pv)+"/"+str(ennemi.get_pvmax())
	var percent_health = float(ennemi.current_pv)/float(ennemi.get_pvmax())
	if(ennemi.current_pv>(ennemi.get_pvmax()/2)): # plus de 50% de pv
		ennemiPVProgressBar.modulate = Color(0,1,0)
	elif(ennemi.current_pv>(ennemi.get_pvmax()/10)):
		ennemiPVProgressBar.modulate = Color(1,1,0)
	else:
		ennemiPVProgressBar.modulate = Color(1,0,0)
	ennemiPVProgressBar.value = percent_health*100


#---------------------------------------------------------------------------------------------------
func update_pe_allie():
	$Allie/VBoxContainer/HBoxContainer/pe.text = "  PE: "+str(allie.current_pe)+"/"+str(allie.get_pemax())

func update_pe_ennemi():
	$Ennemi/HBoxContainer/pe.text = "  PE: "+str(ennemi.current_pe)+"/"+str(ennemi.get_pemax())


#---------------------------------------------------------------------------------------------------
# met a jour les infos de l'allie et de l'ennemi
func update_both_infos_label():
	var container_child_array = $Allie/VBoxContainer/HBoxContainer.get_children()
	container_child_array[0].text = allie.get_name()+" Lvl "+str(allie.level)
	container_child_array[1].text = "  PV: "+str(allie.current_pv)+"/"+str(allie.get_pvmax())
	container_child_array[2].text = "  PE: "+str(allie.current_pe)+"/"+str(allie.get_pemax())
	container_child_array[3].text = allie.get_string_rarity()
	$Allie/VBoxContainer/HBoxContainer2/type1.text = allie.get_type1()
	$Allie/VBoxContainer/HBoxContainer3/type2.text = allie.get_type2()
	
	container_child_array = $Ennemi/HBoxContainer.get_children()
	container_child_array[0].text = ennemi.get_name()+" Lvl "+str(ennemi.level)
	container_child_array[1].text = "  PV: "+str(ennemi.current_pv)+"/"+str(ennemi.get_pvmax())
	container_child_array[2].text = "  PE: "+str(ennemi.current_pe)+"/"+str(ennemi.get_pemax())
	container_child_array[3].text = ennemi.get_type1()+","+ennemi.get_type2()
	$Ennemi/HBoxContainer/rarityLabel.text = ennemi.get_string_rarity()
	
	update_pv_allie()
	update_pv_ennemi()


#---------------------------------------------------------------------------------------------------
# remise a l'état normal ou le joueur peut choisir dans le menu
func display_menu_player():
	menu_principal.visible = true
	menu_capacites.visible = false
	#$Description.visible = false
	allie_have_already_attacked = false
	ennemi_have_already_attacked = false


# who_win = 0 si allie, 1 si ennemi
# fin de la bataille entière
func battle_finished(who_win:int):
	battle_is_finished = true
	menu_principal.visible = false
	menu_capacites.visible = false
	if(who_win==0):# si allie gagne
		if(is_elemon_join_team):
			if(is_any_place_in_the_team):
				$Description/Label.text = ennemi.get_name()+" a rejoint votre équipe."
			else:
				$Description/Label.text = "pas de place dans l'équipe"
		else:
			$Description/Label.text = ennemi.get_name()+" est K.O.\n"
		is_player_win = true
		anim.play("fade_of_ennemi_ko")
		gain_xp()
	else:
		$Description/Label.text = allie.get_name()+" est K.O.\n"
		is_player_win = false
		anim.play("fade_of_allie_ko")


# méthode appelé a la fin d'une animation
func gain_xp():
	PlayerInfo.add_xp_rank(1)
	PlayerInfo.researcher_increase_percent()
	PlayerInfo.money += ennemi.level
	
	if(allie.level>=PlayerInfo.get_elemonLevelLimit()):
		$Description/Label.text += "Niveau limité"
	
	xp_gained = int(float(ennemi.calculate_xp_when_ko()+nbTour*10)*PlayerInfo.xp_gain)
	xp_spe_gained = int(float(ennemi.level*3)*PlayerInfo.xp_spe_gain)
	base_xp = allie.xp
	base_xp_spe = allie.xp_spe
	xp_increasing = true # lance dans _process() le gain d'xp


func maj_xp_label_allie():
	xpBar.value = float(allie.xp)/float(allie.calculate_next_level_xp())*100
	xp_label.text = str(allie.xp)+"/"+str(allie.calculate_next_level_xp())+" xp"
	xpSpeBar.value = float(allie.xp_spe)/float(allie.calculate_next_level_xp_spe())*100
	xp_spe_label.text = str(allie.xp_spe)+"/"+str(allie.calculate_next_level_xp_spe())+" xp spe"

#---------------------------------------------------------------------------------------------------
# affiche les stats avec l'augmentation si monté de niveau
func display_level_up():
	# maj du label
	$Allie/VBoxContainer/HBoxContainer/allieName.text = allie.get_name()+" Lvl "+str(allie.level)
	
	displaying_level_up = 1
	
	$Control.visible=false
	
	$LevelUp.visible = true
	var labels_array = $LevelUp/NinePatchRect/VBoxContainer.get_children()
	labels_array[0].text = "PVmax: "+str(allie.get_pvmax()-2)+" + 2"
	labels_array[1].text = "PEmax: "+str(allie.get_pemax()-1)+" + 1"
	labels_array[2].text = "Att: "+str(allie.get_att()-1)+" + 1"
	labels_array[3].text = "Def: "+str(allie.get_def()-1)+" + 1"
	labels_array[4].text = "AttSpe: "+str(allie.get_attspe()-1)+" + 1"
	labels_array[5].text = "DefSpe: "+str(allie.get_defspe()-1)+" + 1"
	labels_array[6].text = "Vit: "+str(allie.get_speed()-1)+" + 1"

# affiche les nouvelles valeurs des stats après monté de niveau
func display_new_stats_level_up():
	displaying_level_up = 0
	
	var labels_array = $LevelUp/NinePatchRect/VBoxContainer.get_children()
	labels_array[0].text = "PVmax: "+str(allie.pvmax)
	labels_array[1].text = "PEmax: "+str(allie.pemax)
	labels_array[2].text = "Att: "+str(allie.att)
	labels_array[3].text = "Def: "+str(allie.def)
	labels_array[4].text = "AttSpe: "+str(allie.attspe)
	labels_array[5].text = "DefSpe: "+str(allie.defspe)
	labels_array[6].text = "Vit: "+str(allie.speed)
	
	update_pv_allie()
	update_pe_allie()

#---------------------------------------------------------------------------------------------------
# BAG
# partie concernant le bag du joueur
func _on_ObjetButton_pressed():
	if(!$LevelSpeUp.visible):
		if(is_trainer_battle):
			$PlayerBag.first_load(2)
		else:
			$PlayerBag.first_load(1)

func _on_gift_used(gift_name:String): # fonction appélé quand le joueur utilise un gift
	PlayerInfo.bag_gifts[gift_name] -= 1
	var ennemi_power = float(ennemi.current_pv)/float(ennemi.pvmax)*float(ennemi.level)*10.0
	var join_proba = PlayerInfo.get_convinceAbility()*PlayerInfo.bag_gifts_power[gift_name]-ennemi_power
	
	var random_gen = RandomNumberGenerator.new()
	random_gen.randomize()
	var random_number = random_gen.randi_range(0, 100)
	
	if(random_number < join_proba):
		is_any_place_in_the_team = PlayerInfo.add_new_elemon_in_the_player_team(ennemi.id, ennemi.level, ennemi.rarity, ennemi.idcapas)
		$PlayerBag.visible = false
		is_elemon_join_team = true
		battle_finished(Who.ALLIE)


func _on_treatment_used():
	update_pv_allie()
	update_pe_allie()

#---------------------------------------------------------------------------------------------------
# TEAM MANAGER
# aller
func _on_EquipeButton_pressed():
	if(!$LevelSpeUp.visible):
		$ColorRect.visible = true
		menu_principal.visible = false
		team_manager.visible = true
		
		update_team_display()

# retour
func _on_TeamReturnButton_pressed():
	team_manager.visible = false
	menu_principal.visible = true
	$ColorRect.visible = false

# met a jour l'affichage de l'équipe
func update_team_display():
	var team_labels:Array = $TeamManager/HBoxContainer/labels.get_children()
	var team_buttons:Array = $TeamManager/HBoxContainer/buttons.get_children()
	var progress_bars:Array = $TeamManager/HBoxContainer/progressBars.get_children()
	for i in range(allies.size()):
		if allies[i]!=null:
			team_buttons[i].disabled = false
			team_labels[i].text = allies[i].get_name() + " Niv." + str(allies[i].level)
			team_labels[i].text += " PV:" + str(allies[i].current_pv) + "/" + str(allies[i].get_pvmax())
			team_labels[i].text += "\nPE:" + str(allies[i].current_pe) + "/" + str(allies[i].get_pemax())
			# pv progress bars
			progress_bars[2*i].value = float(allies[i].current_pv)/float(allies[i].get_pvmax())*100
			# pe progress bars
			progress_bars[2*i+1].value = float(allies[i].current_pe)/float(allies[i].get_pemax())*100
		else:
			team_labels[i].text = "empty\nslot"
			team_buttons[i].disabled = true
		if i_current_ally==i:
			team_buttons[i].disabled = true


var id_slot_exchange:int
# bouton dans le menu pour échanger de combattant
func _on_ExchangeButton_pressed(id_slot:int):
	$TeamManager/ConfirmationDialog.popup_centered()
	$TeamManager/ConfirmationDialog.dialog_text = "Échanger " + allies[i_current_ally].get_name() + " avec " + allies[id_slot-1].get_name() + "?"
	id_slot_exchange = id_slot

func _on_ConfirmationDialog_confirmed():
	i_current_ally = id_slot_exchange-1 # car i_current_ally commence a 0
	allie = allies[i_current_ally]
	update_elemon_allie() # update affichage pour nouvel allie
	maj_xp_label_allie()
	display_menu_player()
	_on_TeamReturnButton_pressed() # retour au menu principal



#---------------------------------------------------------------------------------------------------
# LEVEL SPE
var chosen_stat:int

func display_level_spe_up():
	$LevelSpeUp.visible = true
	$LevelSpeUp/VBoxContainer/pv/label.text = "PVmax: "+str(allie.pvmax)
	$LevelSpeUp/VBoxContainer/pe/label.text = "PEmax: "+str(allie.pemax)
	$LevelSpeUp/VBoxContainer/att/label.text = "Att: "+str(allie.att)
	$LevelSpeUp/VBoxContainer/def/label.text = "Def: "+str(allie.def) 
	$LevelSpeUp/VBoxContainer/attspe/label.text = "AttSpe: "+str(allie.attspe)
	$LevelSpeUp/VBoxContainer/defspe/label.text = "DefSpe: "+str(allie.defspe)
	$LevelSpeUp/VBoxContainer/speed/label.text = "Vit: "+str(allie.speed)


onready var statsLabels = [$LevelSpeUp/VBoxContainer/pv/label, $LevelSpeUp/VBoxContainer/pe/label, 
	$LevelSpeUp/VBoxContainer/att/label, $LevelSpeUp/VBoxContainer/def/label, 
	$LevelSpeUp/VBoxContainer/attspe/label, $LevelSpeUp/VBoxContainer/defspe/label, 
	$LevelSpeUp/VBoxContainer/speed/label
]
func _on_plusButton_pressed(stat_to_increase):
	chosen_stat = stat_to_increase
	for i in range(0,7):
		if i==stat_to_increase-1:
			statsLabels[i].text = statsLabels[i].text if statsLabels[i].text.ends_with(" + 1") else statsLabels[i].text+" + 1"
		else:
			statsLabels[i].text = statsLabels[i].text.trim_suffix(" + 1") # supprime le "+ 1" à la fin de la String


# validation de la stat changé
func _on_levelSpeOkButton_pressed():
	$LevelSpeUp.visible = false
	match chosen_stat:
		1: # pv
			allie.pvmax += 1
			allie.pvmaxBonus += 1
			allie.current_pv += 1
			update_pv_allie()
		2: # pe
			allie.pemax += 1
			allie.pemaxBonus += 1
			allie.current_pe += 1
			update_pe_allie()
		3: # att
			allie.att += 1
			allie.attBonus += 1
		4: # def
			allie.def += 1
			allie.defBonus += 1
		5: # attspe
			allie.attspe += 1
			allie.attspeBonus += 1
		6: # defspe
			allie.defspe += 1
			allie.defspeBonus += 1
		7: # speed
			allie.speed += 1
			allie.speedBonus += 1
