extends CanvasLayer

const TILE_SIZE = 16

onready var dialogControl:Control = $Control
onready var dialogLabel:Label = $Control/Label
onready var nextTexture:TextureRect = $next

var current_text:int
var player
var percent_time_between_text:float
var texts:Array

# Called when the node enters the scene tree for the first time.
func _ready():
	dialogControl.visible = false
	nextTexture.visible = false


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if dialogControl.visible:
		percent_time_between_text += (4*delta)
		dialogLabel.percent_visible = percent_time_between_text
		if percent_time_between_text >= 1:
			nextTexture.visible = true
	# traitement de l'input pour passer au texte suivant
	if nextTexture.visible and Input.is_action_just_pressed("ok"):
		current_text += 1
		if current_text >= texts.size():# end of dialog
			dialogControl.visible = false
			end_of_dialog()
		else:
			dialogLabel.text = texts[current_text]
			percent_time_between_text = 0
			dialogLabel.percent_visible = 0
		nextTexture.visible = false


# initialise le text des dialogues avec le paramètre dialogs
func load_dialog(dialogs:Array):
	if !dialogs.empty():
		if !dialogControl.visible:
			texts = dialogs
			current_text = 0
			dialogControl.visible = true
			dialogLabel.text = texts[current_text]
	else:
		end_of_dialog()


func end_of_dialog():
	player = Utils.get_player()
	player.begin_fight = true
	player.stop_input_for_pnj = false
