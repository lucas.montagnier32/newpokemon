extends Node2D

# animation de l'herbe
onready var anim_grass = $AnimationPlayer
# chargement de la texture de la grass overlay
const grass_overlay_texture = preload("res://Assets/Grass/stepped_tall_grass.png")
# chargement de la scene grass_step_effect
const GrassStepEffect = preload("res://GrassStepEffect.tscn")

var grass_overlay: TextureRect = null
# vrai si le player est dans l'herbe
var player_inside:bool = false

#---------------------------------------------------------------------------------------------------
# Called when the node enters the scene tree for the first time.
func _ready():
	# connection des signaux du noeud Player a ce script
	# si le signal (premier parametre de la fonction connect) est émis, ce script appelera la fonction du dernier paramètre
	# recuperation du noeud Player de la scene la derniere ajouter (.back) dans CurrentScene
	var player = Utils.get_player()
	#TODO:recupération de la grass_overlay de l'herbe dans laquelle le perso est quand il va dans la BattleScreen

	# remet la variable player_inside a vrai quand il revient de BattleScreen
	player.connect("player_moving_signal", self, "player_exiting_grass")
	player.connect("player_stopped_signal", self, "player_in_grass")

#---------------------------------------------------------------------------------------------------
# fonction appelé quand le signal player_moving_signal est émis
func player_exiting_grass():
	player_inside = false
	Utils.get_player().is_in_grass = false
	if (is_instance_valid(grass_overlay)):
		# suppresion de la grass_overlay quand le joueur sort de l'herbe
		grass_overlay.queue_free()

#---------------------------------------------------------------------------------------------------
# fonction appelé quand le signal player_stopepd_signal est émis
func player_in_grass():
	# TODO ajouter un signal partant de Player quand percent_moving>=0.7 pour lancer le grass_step_effect
	if (player_inside == true):
		var add_where = get_tree().current_scene.get_child(0).get_child(0).get_node("./YSort")
		# init grass_step_effect
		# position est la position actuelle du noeud this
		var grass_step_effect = GrassStepEffect.instance()
		grass_step_effect.position = position
		add_where.add_child(grass_step_effect)

		# init de la texture du node grass_overlay
		# position est la position actuelle du noeud this
		grass_overlay = TextureRect.new()
		grass_overlay.texture = grass_overlay_texture
		grass_overlay.rect_position = position
		# get la scene courante pour ajouter la grass_overlay (ajouter dans le Ysort de Town)
		add_where.add_child(grass_overlay)

#---------------------------------------------------------------------------------------------------
# fonction appelé quand un corps est en collision avec le CollisionShape2D
func _on_Area2D_body_entered(_body):
	player_inside = true
	Utils.get_player().is_in_grass = true
	# change l'animation de l'herbe
	anim_grass.play("Stepped")
