extends Control

const button_normal = preload("res://Assets/UI/button_normal.png")
const button_hover = preload("res://Assets/UI/button_hover.png")
const button_pressed = preload("res://Assets/UI/button_touch.png")

onready var contentButtonNode = $ScrollContainer/content/contentButton
onready var infoLabel = $otherInfos
onready var names = $ScrollContainer/content/contentText/names
onready var values = $ScrollContainer/content/contentText/values

signal gift_used(name) # signal pour informer BattleSreen que le joueur utilise un gift et agir en conséquence
signal treatment_used() # signal pour informer BattleSreen

var actual_situation:int # 0: nothing, 1: simple_battle, 2: trainer_battle
# les variables suivantes servent pour l'affichage dans la fonction load_bag()
var bag_content_size:int
var button:Control
var button_key_used:String
var button_number_used:int
var chosen_elemon:Fightermon.Fightermon_class
var content_part_used:int
var keys_array:Array
var place_to_put_new_tech:int
var values_array:Array

# Called when the node enters the scene tree for the first time.
func _ready():
	self.visible = false
	$Team.visible = false
	$replaceCapaControl.visible = false
	infoLabel.text = ""

func first_load(situation):
	self.visible = true
	actual_situation = situation
	load_bag(PlayerInfo.bag_treatment, 1)

func load_bag(content:Dictionary, contentPart:int) -> void:
	infoLabel.text = ""
	highlight_header_button(contentPart)
	$Team.visible = false
	$replaceCapaControl.visible = false
	bag_content_size = content.size()
	$ScrollContainer/content.rect_min_size = Vector2(144,bag_content_size*26)
	for n in contentButtonNode.get_children(): # suppressions des anciens boutons
		contentButtonNode.remove_child(n)
		n.queue_free()
	names.text = ""
	values.text = ""
	keys_array = content.keys()
	values_array = content.values()
	for i in range(0,bag_content_size):
		names.text += keys_array[i]+"\n"
		if contentPart!=3 && contentPart!=5: # pas de nombre si technique ou utilitaire
			values.text += str(values_array[i])+"\n"
		else:
			values.text = ""
		# création des boutons
		button = TextureButton.new()
		button.expand = true
		button.texture_normal = button_normal
		button.texture_hover = button_hover
		button.texture_pressed = button_pressed
		button.rect_min_size = Vector2(144,18)
		button.mouse_default_cursor_shape = Control.CURSOR_POINTING_HAND
		# warning-ignore:return_value_discarded
		button.connect("pressed", self, "_on_objectButton_pressed", [ contentPart, keys_array[i] , i ])
		contentButtonNode.add_child(button)


func load_team_control():
	$Team.visible = true
	var player_elemons = PlayerInfo.get_all_elemons()
	var labels = $Team/labels.get_children()
	
	for i in range(0, labels.size()):
		if(player_elemons[i]!=null):
			labels[i].text = player_elemons[i].get_name()+"  Niv "+str(player_elemons[i].level)+"\nPV "+str(player_elemons[i].current_pv)+"/"+str(player_elemons[i].pvmax)+"  PE "+str(player_elemons[i].current_pe)+"/"+str(player_elemons[i].pemax)
		else:
			labels[i].text = "empty"

func highlight_header_button(number:int):
	var header_buttons = $headerControl/header.get_children()
	for i in range(0, header_buttons.size()):
		if i==number-1:
			header_buttons[i].modulate = Color.darkorange
		else:
			header_buttons[i].modulate = Color.white

#---------------------------------------------------------------------------------------------------
# button action
func _on_return_pressed():
	self.visible = false

func _on_treatmentButton_pressed():
	load_bag(PlayerInfo.bag_treatment, 1)

func _on_giftsButton_pressed():
	load_bag(PlayerInfo.bag_gifts, 2)

func _on_techButton_pressed():
	load_bag(PlayerInfo.bag_capacites, 3)

func _on_objectsButton_pressed():
	load_bag(PlayerInfo.bag_objects, 4)

func _on_utilityButton_pressed():
	load_bag(PlayerInfo.bag_utility, 5)


# clic sur un des boutons pour utiliser ou voir un objet de sac
func _on_objectButton_pressed(content_part:int, button_key:String, button_number:int):
	infoLabel.text = ""
	content_part_used = content_part
	button_key_used = button_key
	# supprime le modulate darkblue sur l'ancien bouton cliqué
	$ScrollContainer/content/contentButton.get_children()[button_number_used].modulate = Color.white
	button_number_used = button_number
	$ScrollContainer/content/contentButton.get_children()[button_number].modulate = Color.steelblue
	match content_part:
		1: # bag_treatment
			if "pv" in button_key_used:
				infoLabel.text = "Restaure "+str(PlayerInfo.bag_treatment_power[button_key_used])+" PV"
			elif "pe" in button_key_used:
				infoLabel.text = "Restaure "+str(PlayerInfo.bag_treatment_power[button_key_used])+" PE"
			if(PlayerInfo.bag_treatment[button_key_used]>0):
				load_team_control()
		2: # bag_gifts
			infoLabel.text = "Permet de faire venir un elemon dans ton équipe"
			if(actual_situation==1 && PlayerInfo.bag_gifts[button_key_used]>0):
				$ConfirmationDialog.dialog_text = "Est-tu sûr d'utiliser un "+button_key_used+" cadeau ?"
				$ConfirmationDialog.popup_centered()
		3: # bag_capacites
			infoLabel.text = "Permet d'apprendre la technique "+Fightermon.capacites_fight[PlayerInfo.bag_capacites[button_key]][0]+" a un elemon"
			load_team_control()
		4: # bag_objects
			pass
		5: # bag_utility
			pass


# envoie le signal a BattleSreen
func _on_ConfirmationDialog_confirmed():
	emit_signal("gift_used", button_key_used)


func _on_Teambutton_pressed(elemon_number:int):
	chosen_elemon = PlayerInfo.get_elemon(elemon_number)
	if(chosen_elemon!=null):
		match content_part_used:
			1: # treatment
				if "pv" in button_key_used:
					$ConfirmTreatment.dialog_text = "Utiliser "+button_key_used+" sur "+chosen_elemon.get_name()+"?\nIl gagnera "+str(min(chosen_elemon.pvmax-chosen_elemon.current_pv, PlayerInfo.bag_treatment_power[button_key_used]))+" PV"
				elif "pe" in button_key_used:
					$ConfirmTreatment.dialog_text = "Utiliser "+button_key_used+" sur "+chosen_elemon.get_name()+"?\nIl gagnera "+str(min(chosen_elemon.pemax-chosen_elemon.current_pe, PlayerInfo.bag_treatment_power[button_key_used]))+" PE"
				$ConfirmTreatment.popup_centered()
			3: # learn techniques
				if chosen_elemon.capacites.size()<4:
					place_to_put_new_tech = 0
					$ConfirmLearnTechnique.dialog_text = "Apprendre "+Fightermon.capacites_fight[PlayerInfo.bag_capacites[button_key_used]][0]+" a "+chosen_elemon.get_name()+" ?"
					$ConfirmLearnTechnique.popup_centered()
				else:
					$replaceCapaControl/buttons/text/name1.text = chosen_elemon.capacites[0][0]+" Type:"+chosen_elemon.capacites[0][1]+" P:"+str(chosen_elemon.capacites[0][3])
					$replaceCapaControl/buttons/text/name2.text = chosen_elemon.capacites[1][0]+" Type:"+chosen_elemon.capacites[1][1]+" P:"+str(chosen_elemon.capacites[1][3])
					$replaceCapaControl/buttons/text/name3.text = chosen_elemon.capacites[2][0]+" Type:"+chosen_elemon.capacites[2][1]+" P:"+str(chosen_elemon.capacites[2][3])
					$replaceCapaControl/buttons/text/name4.text = chosen_elemon.capacites[3][0]+" Type:"+chosen_elemon.capacites[3][1]+" P:"+str(chosen_elemon.capacites[3][3])
					$replaceCapaControl.visible = true
			_: print("error on _on_Teambutton_pressed contentPart="+str(content_part_used))



func _on_ConfirmTreatment_confirmed():
	if "pv" in button_key_used:
		chosen_elemon.current_pv += int(min(chosen_elemon.pvmax-chosen_elemon.current_pv, PlayerInfo.bag_treatment_power[button_key_used]))
	elif "pe" in button_key_used:
		chosen_elemon.current_pe += int(min(chosen_elemon.pemax-chosen_elemon.current_pe, PlayerInfo.bag_treatment_power[button_key_used]))
	# update
	PlayerInfo.bag_treatment[button_key_used] -= 1
	load_bag(PlayerInfo.bag_treatment, 1)
	load_team_control()
	Utils.get_GUI().update_GUI()
	emit_signal("treatment_used")


func _on_ConfirmLearnTechnique_confirmed():
	chosen_elemon.set_capa_from_id(PlayerInfo.bag_capacites[button_key_used], place_to_put_new_tech) # seras mit à la première place libre
	infoLabel.text = chosen_elemon.get_name()+" a appris "+Fightermon.capacites_fight[PlayerInfo.bag_capacites[button_key_used]][0]
	$replaceCapaControl.visible = false

func _on_replaceCapaButton_pressed(id_button:int):
	place_to_put_new_tech = id_button
	$ConfirmLearnTechnique.dialog_text = "Apprendre "+Fightermon.capacites_fight[PlayerInfo.bag_capacites[button_key_used]][0]+" a "+chosen_elemon.get_name()+"\nà la place de "+chosen_elemon.capacites[id_button][0]+" ?"
	$ConfirmLearnTechnique.popup_centered()
